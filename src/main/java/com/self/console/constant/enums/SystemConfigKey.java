package com.self.console.constant.enums;

/**
 * 系统配置项
 */
public enum SystemConfigKey {
	
	MAX_LOGIN_COUNT("loginFailMaxCount", "登录错误最大次数"),
	CHECK_AUTH_CODE("checkAuthCode", "是否启用动态密码验证");

	private SystemConfigKey(String value, String memo) {
		this.value = value;
		this.memo = memo;
	}

	private String value;
	private String memo;

	public String getValue() {
		return value;
	}

	public String getMemo() {
		return memo;
	}
}
