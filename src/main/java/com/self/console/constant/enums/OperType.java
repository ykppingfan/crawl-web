package com.self.console.constant.enums;

/**
 * 操作类型
 */
public enum OperType {
	
	LOGIN(0L, "doLogin", "登录"), 
	LOGOUT(1L, "logout", "登出"),
	CREATE(2L, "create", "新增"),
	UPDATE(3L, "update", "更改"),
	DESTORY(4L, "destory", "删除");
	
	private Long value;
	private String oper;
	private String name;

	private OperType(Long value, String oper, String name) {
		this.value = value;
		this.oper = oper;
		this.name = name;
	}

	public Long getValue() {
		return value;
	}
	
	public String getOper() {
		return oper;
	}

	public String getName() {
		return name;
	}
	
	public static Long getValue(String oper) {
		for(OperType type : OperType.values()){
			if(type.equals(oper)){
				return type.getValue();
			}
		}
		return null;
	}
	
	public boolean equals(String oper) {
		return this.oper.equals(oper);
	}
}
