package com.self.console.constant.enums;

/**
 * 登录登出类型
 */
public enum LogonType {
	
	LOGIN(0, "登录"), LOGOUT(1, "登出");
	
	private Integer value;
	private String name;

	private LogonType(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
