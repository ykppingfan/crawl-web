package com.self.console.constant.enums;

/**
 * 系统角色
 */
public enum SystemRole {

	ROOT(-1L, "系统管理员"), ADMIN(1L, "管理员"), UI(4L, "UI人员"), EDITOR(5L, "编辑");

	private Long value;

	private String name;

	private SystemRole(Long value, String name) {
		this.value = value;
		this.name = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
