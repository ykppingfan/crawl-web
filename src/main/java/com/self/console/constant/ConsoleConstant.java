package com.self.console.constant;

import java.io.File;

public class ConsoleConstant {

	public static final char SPT = '/';							// 路径分隔符
	
	public static final char FILE_SPT = File.separatorChar;		// 系统路径分隔符
	
	public static final Long SUPER_ROLE = -1L;					// 超级管理员角色

	public static String CONSOLE_DOMAIN = null;					// 控制台域名
	
	public static Long SECRET_KEY = null;							// 谷歌密钥加解密私钥
	
}
