package com.self.console.constant;

/**
 * 用户中心缓存常量
 * ClassName: CacheType <br/> 
 * @author liliang 2014-9-30 上午10:24:17 <br/> 
 * @version 4.0.0
 */
public enum CacheType {

	FIVE_SECOND_STORE("fiveSecondStore", "五秒缓存"),
	THIRTY_SECOND_STORE("thirtySecondStore", "三十秒缓存"),
	ONE_MINUTE_STORE("oneMinuteStore", "一分钟缓存"),
	FIVE_MINUTE_STORE("fiveMinuteStore", "五分钟缓存"),
	TEN_MINUTE_STORE("tenMinuteStore", "十分钟缓存"),
	THIRTY_MINUTE_STORE("thirtyMinuteStore", "三十分钟缓存"),
	ONE_HOUR_STORE("oneHourStore", "一小时缓存"),
	TWO_HOUR_STORE("twoHourStore", "两小时缓存"),
	THREE_HOUR_STORE("threeHourStore", "三小时缓存"),
	SIX_HOUR_STORE("sixHourStore", "六小时缓存"),
	TWELVE_HOUR_STORE("twelveHourStore", "十二小时缓存"),
	ONE_DAY_STORE("oneDayStore", "一天缓存"),
	TWO_DAY_STORE("twoDayStore", "两天缓存"),
	THREE_DAY_STORE("threeDayStore", "三天缓存"),
	TEN_DAY_STORE("tenDayStore", "十天缓存"),
	SESSION_STORE("sessionStore", "session缓存");
	
	private String value;
	private String name;
	
	private CacheType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
