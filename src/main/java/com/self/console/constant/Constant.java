package com.self.console.constant;

/**
 * Created by Administrator on 2016/6/14.
 */
public class Constant {

    public static final String RESOURCE_UPLOAD_PATH = "/upload/p2p";

    public static final String DEFAULT_PLATFORM_LOGO = "/p2p/cngold/img/p2p_logo6.gif"; // 平台默认logo

    public static final String PRODUCT_BUYLOG = "product.buylog";

    public static final String PRODUCT_ID_LIST = "product.id.list";

    public static final String PRODUCT_VISIT_COUNT = "product_visit_count";

    public static String RESOURCE_DOMAIN_DIR_PATH = null;

    public static final String PLATFORM_ID_LIST = "platform.id.list";

    public static final String PLATFORM_VISIT_COUNT = "platform_visit_count";

    public enum ResultCode {
        CRAWL_PART_ERROR((short)0,"部分成功"),
        QUOTE_SYNC((short)1,"行情同步"),
        CRAWL_OK((short)200,"全部成功"),
        INTERPRETER_NOT_FOUND((short)9400,"解析器未找到"),
        CRAWL_ERROR((short)9404,"全部失败"),
        FTP_ERROR((short)9500,"FTP上传失败"),
        INTERNAL_ERROR((short)9501,"内容解析出错"),
        PROXY_NULL((short)9600,"代理异常"),
        PROXY_EXCEED((short)9601,"使用足够数量的代理都未采集到结果"),
        ARTICLE_URL_EXCEPTION((short)9701,"文章URL解析失败"),
        EXCEPTION_CODE((short)-9999,"抓取页面异常"),
        EXCEPTION_CONTENT_CODE((short)-9000,"http请求结果为空"),
        CONTENT_EXCEED_CODE((short)-9009,"抓取页面内容超出最大长度");


        private short value;
        private String name;

        private ResultCode(short value,String name){
            this.value = value;
            this.name = name;
        }
        public short getValue() {
            return value;
        }
        public void setValue(short value) {
            this.value = value;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }

}
