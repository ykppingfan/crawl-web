package com.self.console.constant;

/**
 * 操作权限常量
 */
public class OperCodeConst {

	/** 系统管理 **************************************/
	public static final String OPER_99_1_0 = "99_1_0";
	
	/** 系统管理员_列表 */
	public static final String OPER_99_1_1 = "99_1_1";
	/** 系统管理员_新建 */
	public static final String OPER_99_1_2 = "99_1_2";
	/** 系统管理员_修改 */
	public static final String OPER_99_1_3 = "99_1_3";
	/** 系统管理员_删除 */
	public static final String OPER_99_1_4 = "99_1_4";

	/** 角色管理 */
	public static final String OPER_99_2_1 = "99_2_1";
	/** 角色_新建 */
	public static final String OPER_99_2_2 = "99_2_2";
	/** 角色_修改 */
	public static final String OPER_99_2_3 = "99_2_3";
	/** 角色_删除 */
	public static final String OPER_99_2_4 = "99_2_4";

	/** 系统配置管理 */
	public static final String OPER_99_3_1 = "99_3_1";
	/** 系统配置_新建 */
	public static final String OPER_99_3_2 = "99_3_2";
	/** 系统配置_修改 */
	public static final String OPER_99_3_3 = "99_3_3";
	/** 系统配置_删除 */
	public static final String OPER_99_3_4 = "99_3_4";

	/** 系统日志管理 */
	public static final String OPER_99_4_1 = "99_4_1";
	/** 系统日志_查看 */
	public static final String OPER_99_4_2 = "99_4_2";
	/** 系统日志_删除 */
	public static final String OPER_99_4_4 = "99_4_4";
	
	/** 贷款管理 - 基础数据 *********************** */
	/** 地区管理 */
	public static final String OPER_1_1_1 = "1_1_1"; // 查看
	public static final String OPER_1_1_2 = "1_1_2"; // 同步
	public static final String OPER_1_1_3 = "1_1_3"; // 修改
	public static final String OPER_1_1_4 = "1_1_4"; // 删除
	
	/** 平台管理 */
	public static final String OPER_2_1_1 = "2_1_1"; // 查看
	public static final String OPER_2_1_2 = "2_1_2"; // 新增
	public static final String OPER_2_1_3 = "2_1_3"; // 修改、置顶、推荐
	public static final String OPER_2_1_4 = "2_1_4"; // 删除
	
	/** 平台标签管理 */
	public static final String OPER_2_2_1 = "2_2_1"; // 查看、查看机构关联
	public static final String OPER_2_2_2 = "2_2_2"; // 新增
	public static final String OPER_2_2_3 = "2_2_3"; // 修改、添加机构关联、删除机构关联
	public static final String OPER_2_2_4 = "2_2_4"; // 删除
	
	/** 平台排名管理 */
	public static final String OPER_2_3_1 = "2_3_1"; // 查看
	public static final String OPER_2_3_2 = "2_3_2"; // 新增
	public static final String OPER_2_3_3 = "2_3_3"; // 修改
	public static final String OPER_2_3_4 = "2_3_4"; // 删除
	
	/** 平台行情管理 */
	public static final String OPER_2_4_1 = "2_4_1"; // 查看
	public static final String OPER_2_4_4 = "2_4_4"; // 删除
	
	/** 产品管理 */
	public static final String OPER_3_1_1 = "3_1_1"; // 查看
	public static final String OPER_3_1_2 = "3_1_2"; // 新增、导入
	public static final String OPER_3_1_3 = "3_1_3"; // 修改、审核
	public static final String OPER_3_1_4 = "3_1_4"; // 删除
	
	/** 贷款管理 - 模版配置 *********************** */
	/** 模版配置 */
	public static final String OPER_4_1_1 = "4_1_1"; // 查询
	public static final String OPER_4_1_2 = "4_1_2"; // 新增
	public static final String OPER_4_1_3 = "4_1_3"; // 修改
	public static final String OPER_4_1_4 = "4_1_4"; // 删除
	
	/** HTML片段管理 */
	public static final String OPER_4_2_1 = "4_2_1"; // 查询
	public static final String OPER_4_2_2 = "4_2_2"; // 新增
	public static final String OPER_4_2_3 = "4_2_3"; // 修改
	public static final String OPER_4_2_4 = "4_2_4"; // 删除
	public static final String OPER_4_2_5 = "4_2_5"; // 生成、上传
	
}
