package com.self.console.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.apache.commons.lang.StringUtils;

/**
 * 
* <p>Title: SerializerUtils</p>
* <p>Description: </p>
* <p>Company: </p>
* @author Administrator
* @date 下午8:45:17
 */
public class SerializerUtils {

    private final static String OBJECT_TYPE_FALG = "@type";

    public static boolean isObject(String value) {
        if (value.indexOf(OBJECT_TYPE_FALG) > 0) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Object> T deserialization(String stringObj) {
        if (StringUtils.isBlank(stringObj)) {
            return null;
        }
        if (!isObject(stringObj)) {
            return (T) stringObj;
        }
        Object v = JSON.parse(stringObj);
        if (v == null) {
            return null;
        }
        return (T) v;

    }

    public static <T extends Object> String serialization(T obj) {
        if (obj == null) {
            return null;
        }
        String v = null;

        if (obj instanceof String) {
            v = (String) obj;
        } else {
            v = JSON.toJSONString(obj, SerializerFeature.WriteClassName);
        }
        return v;
    }
}
