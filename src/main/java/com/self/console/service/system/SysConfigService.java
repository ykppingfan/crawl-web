package com.self.console.service.system;

import java.util.List;

import com.self.console.persistence.mapper.SysConfigMapper;
import com.self.console.persistence.model.SysConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sangame.sow.ssh.orm.hibernate.support.Page;
import com.sangame.sow.ssh.service.result.DefaultResult;
import com.sangame.sow.ssh.service.result.Result;
import com.sangame.sow.ssh.service.result.ResultCode;

@Service
public class SysConfigService {

	@Autowired
	private SysConfigMapper sysConfigMapper;

	public Page<SysConfig> findPage(SysConfig search) {
		Page<SysConfig> result = new Page<SysConfig>();
		result.setPageNo(search.getCurrentPage());
		result.setPageSize(search.getPageSize());

		List<SysConfig> data = sysConfigMapper.findPage(search);
		long totalCount = sysConfigMapper.countPage(search);

		result.setResult(data);
		result.setTotalCount(totalCount);
		return result;
	}

	/**
	 * 添加
	 * 
	 * @param sysConfig
	 * @return
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result create(SysConfig sysConfig) {
		if (sysConfig == null) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "系统配置为空！"));
		}
		if (checkNameUnique(sysConfig)) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "系统配置已存在！"));
		}
		sysConfigMapper.insert(sysConfig);
		return new DefaultResult(new ResultCode(ResultCode.SUCCESS,"添加成功！"));
	}

	/**
	 * 修改
	 * 
	 * @param keyword
	 * @param groupId
	 * @param keywordId
	 * @return
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result update(SysConfig sysConfig, String preConfigKey) {
		if (sysConfig == null) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "系统配置为空！"));
		}
		if (!sysConfig.getConfigKey().equals(preConfigKey) && checkNameUnique(sysConfig)) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "系统配置已经存在！"));
		}
		sysConfigMapper.update(sysConfig);
		return new DefaultResult(new ResultCode(ResultCode.SUCCESS,"修改成功！"));
	}

	public boolean checkNameUnique(SysConfig sysConfig) {
		return sysConfigMapper.countUnique(sysConfig) > 0;
	}

	public SysConfig find(Long id) {
		return sysConfigMapper.find(id);
	}

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result destory(String ids) {
		if (StringUtils.isBlank(ids)) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "没有选择要删除的信息！"));
		}
		String[] sysConfigIDs = ids.split(",");
		for (String id : sysConfigIDs) {
			if (StringUtils.isNotBlank(id)) {
				SysConfig sysConfig = find(Long.valueOf(id));
				if (sysConfig != null) {
					sysConfigMapper.delete(Long.valueOf(id));
				}
			}
		}
		return new DefaultResult(new ResultCode(ResultCode.SUCCESS,"删除成功！"));
	}
	
	public SysConfig findByKey(String key) {
		return sysConfigMapper.findByKey(key);
	}
}