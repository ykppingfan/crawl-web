package com.self.console.service.system;

import java.util.List;

import com.self.console.persistence.mapper.SysLogMapper;
import com.self.console.persistence.model.SysLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sangame.sow.ssh.orm.hibernate.support.Page;
import com.sangame.sow.ssh.service.result.DefaultResult;
import com.sangame.sow.ssh.service.result.Result;
import com.sangame.sow.ssh.service.result.ResultCode;

@Service
public class SysLogService {

	@Autowired
	private SysLogMapper sysLogMapper;

	public Page<SysLog> findPage(SysLog search) {
		Page<SysLog> result = new Page<SysLog>();
		result.setPageNo(search.getCurrentPage());
		result.setPageSize(search.getPageSize());

		List<SysLog> data = sysLogMapper.findPage(search);
		long totalCount = sysLogMapper.countPage(search);

		result.setResult(data);
		result.setTotalCount(totalCount);
		return result;
	}

	/**
	 * 添加
	 * 
	 * @param sysLog
	 * @return
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result create(SysLog sysLog) {
		Result result = new DefaultResult();

		sysLogMapper.insert(sysLog);
		result.setResultCode(new ResultCode(ResultCode.SUCCESS));
		result.setModel(Result.DEFAULT_MODEL_KEY, "添加成功！");
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param keyword
	 * @param groupId
	 * @param keywordId
	 * @return
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result update(SysLog sysLog) {
		Result result = new DefaultResult();
		if (sysLog != null) {
			sysLogMapper.update(sysLog);
			result.setResultCode(new ResultCode(ResultCode.SUCCESS));
			result.setModel(Result.DEFAULT_MODEL_KEY, "更新数据成功！");
		}
		return result;

	}

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public void save(SysLog log) {
		sysLogMapper.insert(log);
	}

	public SysLog find(Long id) {
		SysLog sysLog = sysLogMapper.find(id);
		return sysLog;
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.DEFAULT)
	public Result destory(String ids) {
		if (StringUtils.isBlank(ids)) {
			return new DefaultResult(new ResultCode(ResultCode.FAILURE, "没有选择要删除的信息！"));
		}
		String[] sysLogIDs = ids.split(",");
		for (String id : sysLogIDs) {
			if (StringUtils.isNotBlank(id)) {
				SysLog sysLog = find(Long.valueOf(id));
				if (sysLog != null) {
					sysLogMapper.delete(Long.valueOf(id));
				}
			}
		}
		return new DefaultResult(new ResultCode(ResultCode.SUCCESS, "删除成功！"));
	}

}