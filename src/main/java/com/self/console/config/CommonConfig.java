package com.self.console.config;

import com.sangame.sow.config.BaseConfig;
import com.self.console.constant.Constant;
import org.w3c.dom.Node;

/**
 * 配置文件BEAN 对应 config.xml
 */
public class CommonConfig extends BaseConfig {

	private String appId; // 节点id
	private String version; // 版本号
	private String buildNo; // 构建号
	private String whiteIp;
	private boolean openIpFilter;
	private String resourseDomain;
	private String p2pDomain;

	public CommonConfig(String configFilePath) {
		super(configFilePath);
		load();
	}

	private void load() {
		setVersion(super.getValue("product.version"));
		setBuildNo(super.getValue("build.number"));
		setAppId(super.getValue("app.id"));

		setOpenIpFilter(super.getBooleanValue("open.filter.ip"));
		setWhiteIp(super.getValue("access.filter.ip"));
		setResourseDomain(super.getValue("domain.resource"));
		setP2pDomain(super.getValue("p2p.domain"));
	}

	public String getP2pDomain() {
		return p2pDomain;
	}

	public void setP2pDomain(String p2pDomain) {
		this.p2pDomain = p2pDomain;
	}

	@Override
	protected void loadSpecial(Node n) {
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBuildNo() {
		return buildNo;
	}

	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	public String getWhiteIp() {
		return whiteIp;
	}

	public void setWhiteIp(String whiteIp) {
		this.whiteIp = whiteIp;
	}

	public boolean isOpenIpFilter() {
		return openIpFilter;
	}

	public void setOpenIpFilter(boolean openIpFilter) {
		this.openIpFilter = openIpFilter;
	}

	public String getResourseDomain() {
		return resourseDomain;
	}

	public void setResourseDomain(String resourseDomain) {
		Constant.RESOURCE_DOMAIN_DIR_PATH = resourseDomain;
		this.resourseDomain = resourseDomain;
	}


}
