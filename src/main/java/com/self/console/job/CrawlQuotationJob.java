package com.self.console.job;

import com.self.console.service.CrawlQuotationService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Administrator on 2016/6/15.
 */
public class CrawlQuotationJob extends BaseJob {

    @Autowired
    private CrawlQuotationService service;

    @Override
    public void run() {
        String goldPrice = service.crawlQuotation();
        System.out.println(goldPrice);
    }
}
