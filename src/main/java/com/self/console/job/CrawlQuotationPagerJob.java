package com.self.console.job;

import com.self.console.service.CrawlQuotationPagerService;
import com.self.console.service.CrawlQuotationService;
import com.self.console.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Administrator on 2016/6/15.
 */
public class CrawlQuotationPagerJob extends BaseJob {

    @Autowired
    private CrawlQuotationPagerService service;
      @Autowired
      private RedisService redisService;

    @Override
    public void run() {
        String goldPrice = service.crawlQuotation();
        System.out.println(goldPrice);
        // 购买分为余额支付，一个银行卡支付
//        int j=1;// 定期过滤
//        if (j == 1) {
//            for (int i=0; i<=10; i++) {
//                long lock1 = redisService.setnx("huangjinbao" + "activitylock", "lock");
//                if (lock1 == 1) {
//                    // 执行操作
//                    // 删除锁
//                    break;
//                }
//                if (i == 10) {
//                    // 最后一次取锁失败
//                    // 返回拥挤等提示信息
//                }
//            }
//        }
        long lock1 = redisService.setnx("huangjinbao" + "activitylock", "lock");
        redisService.expire("huangjinbao" + "1111" + "lock", 300);
        if (lock1 == 0) {
            System.out.println("lock1未获取到锁");
        } else {
            System.out.println("lock1获取到锁");
        }
        redisService.del("huangjinbao" + "1111" + "lock");
    }
}
