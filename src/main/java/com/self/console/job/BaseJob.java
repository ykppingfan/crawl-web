package com.self.console.job;


public abstract class BaseJob {

	public static final String APP_JOB_KEY = "CREDIT.JOB.KEY.ykp";

	public static String APP_JOB_VALUE = null;

	public abstract void run() throws Exception;

}

