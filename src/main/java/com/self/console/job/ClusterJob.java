package com.self.console.job;

import com.sangame.sow.cache.CacheHelper;
import com.sangame.sow.cache.constant.CacheType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClusterJob extends BaseJob {

	private static final Logger LOG = LoggerFactory.getLogger(ClusterJob.class);

	@Override
	public void run() {
		if (StringUtils.isBlank(APP_JOB_KEY) || StringUtils.isBlank(APP_JOB_VALUE)) {
			LOG.warn("appJobValue 未初始化！");
			return;
		}
		String cacheValue = (String) CacheHelper.get(CacheType.ONE_MINUTE_STORE.getValue(), CacheHelper.buildKey(APP_JOB_KEY));
		if (StringUtils.isBlank(cacheValue) || cacheValue.equals(APP_JOB_VALUE)) {
			CacheHelper.put(CacheType.ONE_MINUTE_STORE.getValue(), CacheHelper.buildKey(APP_JOB_KEY), APP_JOB_VALUE);
			if (LOG.isDebugEnabled()) {
				LOG.debug("appJobValue缓存已被更新！当前tomcat正在执行job！uuid: " + APP_JOB_VALUE);
			}
			return;
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("当前tomcat没有执行job权限！等待其它tomcat掉线！uuid: " + cacheValue);
		}
	}
}
