package com.self.console.job;

import com.sangame.sow.cache.CacheHelper;
import com.sangame.sow.cache.constant.CacheType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SinglenessJob extends BaseJob {

	private static final Logger LOG = LoggerFactory.getLogger(SinglenessJob.class);

	public abstract void execute() throws Exception;;

	@Override
	public void run() {
		if (StringUtils.isBlank(APP_JOB_KEY) || StringUtils.isBlank(APP_JOB_VALUE)) {
			LOG.warn("appJobValue 未初始化！");
			return;
		}
		try {
            String cacheValue = (String) CacheHelper.get(CacheType.ONE_MINUTE_STORE.getValue(), CacheHelper.buildKey(APP_JOB_KEY));
			if (StringUtils.isNotBlank(cacheValue)	&& cacheValue.equals(APP_JOB_VALUE)) {
                execute();
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}



}
