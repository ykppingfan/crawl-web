package com.self.console.web;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/")
public class SetupController {

	/**
	 * 关于我们
	 * @return
	 * @return ModelAndView
	 * @author jason
	 * @date 2016年5月5日
	 */
	@RequestMapping(method = RequestMethod.GET, value = "about.htm")
	public ModelAndView about(String type) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("type", type); //type标记来源，不为空时是h5来源
		mav.setViewName("setup/about");
		return mav;
	}

}
