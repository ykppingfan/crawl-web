package com.self.console.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.self.console.config.CommonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sangame.sow.ssh.web.springmvc.GenericController;

import freemarker.ext.beans.BeansWrapper;

public class GenericInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private CommonConfig commonConfig;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		GenericController action = (GenericController) handlerMethod.getBean();
		action.setRequest(request);
		action.setResponse(response);
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		if (modelAndView != null) {
			modelAndView.addObject("action", handlerMethod.getBean());
			modelAndView.addObject("contextPath", request.getContextPath());
			modelAndView.addObject("statics", new BeansWrapper().getStaticModels());
//			modelAndView.addObject("webResourcePath", commonConfig.getResourseDomain());
		}
	}
}
