package com.self.console.web.controller.back.common;

import com.self.console.config.CommonConfig;
import com.self.console.constant.ConsoleConstant;
import com.self.console.dto.AdminDto;
import com.self.console.service.system.AdminService;
import com.self.console.web.controller.common.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 所有管理端Action的基类
 */
public class BackController extends BaseController {

	public static final String PRE_URL = "pre_url"; // 执行某个action前所在的页面

	public static final String COMMON_VIEW_NAMESPACE = "/back/common";

	@Autowired
	protected CommonConfig commonConfig;
	@Autowired
	protected AdminService adminService;

	public boolean hasOper(String operCode) {
		AdminDto user = getUser();
		if (user == null) {
			return false;
		}
		// 超级管理员拥有所有权限
		if (ConsoleConstant.SUPER_ROLE.equals(user.getRoleId())){
			return true;
		}
		
		if(StringUtils.isBlank(operCode)) {
			return false;
		}
		
		// 没权限可能操作的对象不在当前站点，去数据库取权限
		if(!user.hasOper(operCode)){
			return false;
		} else {
			return true;
		}
	}
	
	protected String getReturnUrl() {
		String url = (String) getSessionAttribute(PRE_URL);
		if (StringUtils.isBlank(url)) {
			return "javascript:history.back()";
		} else {
			url+="&_r = " + Math.random();
			return url;
		}
	}
	
}
