package com.self.console.web.controller.common;

import com.self.console.config.CommonConfig;
import com.self.console.constant.ConsoleConstant;
import com.self.console.dto.AdminDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.sangame.sow.ssh.service.result.Result;
import com.sangame.sow.ssh.web.springmvc.GenericController;
import com.sangame.sow.utils.SshStringUtils;

import freemarker.template.Configuration;

/**
 * 所有 Action基类
 */
public abstract class BaseController extends GenericController {

	protected static final Logger LOG = LoggerFactory.getLogger("MESSAGE.ACTION.LOG");

	public static final String JSON_PAGE = "back/common/json";
	public static final String LOGIN_PAGE = "back/common/login";

	public static final String ERROR_PAGE = "back/common/error";
	public static final String TIME_OUT_PAGE = "back/common/timeout";
	public static final String SHOW_MSG_PAGE = "back/common/show_msg";

	public static int PAGE_SIZE = 30;

	@Autowired
	private Configuration configuration;
	@Autowired
	protected CommonConfig commonConfig;

	public CommonConfig getConfig() {
		return commonConfig;
	}

	public String toUtf8Encode(String s) {
		if (s == null)
			return null;
		else
			return SshStringUtils.toUtf8Encode(s);
	}

	public AdminDto getUser() {
		return super.getCurrentSession() == null ? null : (AdminDto) super.getCurrentSession();
	}

	public String getRealPath(String webPath) {
		return getServletContext().getRealPath(webPath);
	}

	public String getRealWebRoot() {
		return getServletContext().getRealPath(String.valueOf(ConsoleConstant.FILE_SPT));
	}

	public Configuration getFreemarkerConfiguration() {
		return configuration;
	}

	/**
	 * Service得到RESULT MAP 赋值给model视图展现
	 * 
	 * @param model
	 * @param result
	 */
	public void resultToModel(Model model, Result result) {
		if (result.getModels() == null || result.getModels().isEmpty()) {
			return;
		}
		model.addAllAttributes(result.getModels());
	}

}
