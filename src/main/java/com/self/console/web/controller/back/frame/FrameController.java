package com.self.console.web.controller.back.frame;

import com.self.console.web.controller.back.common.BackController;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 主界面
 */
@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/back/frame")
public class FrameController extends BackController {

	@RequestMapping("main.htm")
	public String main(ModelMap model) {
		model.addAttribute("currentSession", getCurrentSession());
		model.addAttribute("config", commonConfig);
		return "back/frame/main";
	}

	@RequestMapping("menu.htm")
	public String menu() {
		return "back/frame/menu";
	}

	@RequestMapping("welcome.htm")
	public String welcome(ModelMap model) {
		model.addAttribute("currentSession", getCurrentSession());
		return "back/frame/welcome";
	}

}
