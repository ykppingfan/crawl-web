package com.self.console.persistence.dao.common;

import java.io.Serializable;

import com.google.common.base.CaseFormat;
import com.sangame.sow.ssh.dbroute.QueryRoute;
import com.sangame.sow.ssh.dbroute.QueryType;
import com.sangame.sow.ssh.orm.mybatis.dao.BaseDao;
import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * 公共基础dao，实现了基础的增删改查方法
 * 
 * @param <T>
 * @param <PK>
 */
public abstract class CommonDao<T extends BaseEntity, PK extends Serializable> extends BaseDao<T, PK> {

	protected String lowerCamelToLowerUnderscore(String prefix) {
		return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, prefix);
	}

	protected void setPageParams(int currentPage, int pageSize, T search) {
		search.setCurrentPage(currentPage);
		search.setPageSize(pageSize);
	}

	/**
	 * 删除记录
	 * 
	 * @param id
	 */
	@QueryRoute(type = QueryType.WRITE)
	public int delete(final PK id) {
		return getMapper().delete(id);
	}

	/**
	 * 新增记录
	 * 
	 * @param entity
	 * @return
	 */
	@QueryRoute(type = QueryType.WRITE)
	public void insert(T entity) {
		getMapper().insert(entity);
	}

	/**
	 * 更新记录
	 * 
	 * @param entity
	 */
	@QueryRoute(type = QueryType.WRITE)
	public int update(T entity) {
		return getMapper().update(entity);
	}

}
