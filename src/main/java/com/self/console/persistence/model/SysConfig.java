package com.self.console.persistence.model;

import com.sangame.sow.ssh.annotation.OperLog;
import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * 系统配置表
 */
public class SysConfig extends BaseEntity {

	private static final long serialVersionUID = 7054952259053375485L;

	@OperLog(i18nKey = "配置关键字名称")
	private String configKey;

	@OperLog(i18nKey = "配置值")
	private String configValue;

	@OperLog(i18nKey = "显示名称")
	private String showName;

	@OperLog(i18nKey = "备注")
	private String remark;

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
