package com.self.console.persistence.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.sangame.sow.ssh.annotation.OperLog;
import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * 用户实体类
 */
public class Admin extends BaseEntity {

	private static final long serialVersionUID = -778204799813793114L;

	@OperLog(i18nKey = "用户名")
	private String userName;

	@OperLog(i18nKey = "用户密码")
	private String userPwd;

	@OperLog(i18nKey = "用户组")
	private Long roleId;

	@OperLog(i18nKey = "最后登录IP")
	private String lastLoginIp;

	@OperLog(i18nKey = "最后登录时间")
	private Date lastLoginTime;

	@OperLog(i18nKey = "是否被禁用")
	private Long disableFlag;

	@OperLog(i18nKey = "登录已经失败次数")
	private Long failNumber;

	@OperLog(i18nKey = "Google密钥")
	private String loginKey;
	
	@OperLog(i18nKey = "备注")
	private String remark;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Long getDisableFlag() {
		return disableFlag;
	}

	public void setDisableFlag(Long disableFlag) {
		this.disableFlag = disableFlag;
	}
	
	public Long getFailNumber() {
		return failNumber == null ? 0L : failNumber;
	}

	public void setFailNumber(Long failNumber) {
		this.failNumber = failNumber == null ? 0L : failNumber;
	}
	
	public String getLoginKey() {
		return loginKey;
	}

	public void setLoginKey(String loginKey) {
		this.loginKey = loginKey;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 加载权限点
	 */
	public Set<String> loadOperCode() {
		Set<String> operCodeList = new HashSet<String>();
		// 编辑权限
		return operCodeList;
	}

}
