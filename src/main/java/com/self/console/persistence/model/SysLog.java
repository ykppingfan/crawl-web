package com.self.console.persistence.model;

import com.sangame.sow.ssh.annotation.OperLog;
import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * 系统日志表
 */
public class SysLog extends BaseEntity {

	private static final long serialVersionUID = -6033743483641922611L;

	@OperLog(i18nKey = "管理员Id")
	private Long userId;

	@OperLog(i18nKey = "操作IP")
	private String operIp;
	
	@OperLog(i18nKey = "操作类型")
	private Long operType;

	@OperLog(i18nKey = "操作详述")
	private String operDesc;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getOperIp() {
		return operIp;
	}

	public void setOperIp(String operIp) {
		this.operIp = operIp;
	}
	
	public Long getOperType() {
		return operType;
	}

	public void setOperType(Long operType) {
		this.operType = operType;
	}

	public String getOperDesc() {
		return operDesc;
	}

	public void setOperDesc(String operDesc) {
		this.operDesc = operDesc;
	}

}
