package com.self.console.persistence.model;

import java.util.ArrayList;

public class ConfigResult {
	private ArrayList<ConfigArgs> list;

	public ArrayList<ConfigArgs> getList() {
		return list;
	}

	public void setList(ArrayList<ConfigArgs> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "ConfigResult [list=" + list + "]";
	}
}
