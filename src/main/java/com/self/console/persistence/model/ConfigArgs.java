package com.self.console.persistence.model;

public class ConfigArgs {
	private String pageSize;
	private String commendFlag;
	private String adFlag;
	private String orderBy;

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getCommendFlag() {
		return commendFlag;
	}

	public void setCommendFlag(String commendFlag) {
		this.commendFlag = commendFlag;
	}

	public String getAdFlag() {
		return adFlag;
	}

	public void setAdFlag(String adFlag) {
		this.adFlag = adFlag;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	@Override
	public String toString() {
		return "ConfigArgs [pageSize=" + pageSize + ", commendFlag="
				+ commendFlag + ", adFlag=" + adFlag + ", orderBy=" + orderBy
				+ "]";
	}
}
