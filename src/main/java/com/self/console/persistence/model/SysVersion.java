package com.self.console.persistence.model;

import com.sangame.sow.ssh.annotation.OperLog;
import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * 系统版本表
 */
public class SysVersion extends BaseEntity {

	private static final long serialVersionUID = -8057726970400418749L;

	@OperLog(i18nKey = "版本号")
	private String sysVersion;

	public String getSysVersion() {
		return sysVersion;
	}

	public void setSysVersion(String sysVersion) {
		this.sysVersion = sysVersion;
	}

}
