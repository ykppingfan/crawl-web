package com.self.console.persistence.model;

import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

/**
 * @Description:
 * @author Ethan.li
 * @date 2014-12-15
 */
public class TemplateConfig extends BaseEntity {

	private static final long serialVersionUID = -6474132313974389074L;

	private String configKey;
	
	private String configValue;
	
	private Long type;
	
	private String remark;

    private String configFile;

	private String staticUrl;

	private Long isRemote;

	public Long getIsRemote() {
		return isRemote;
	}

	public void setIsRemote(Long isRemote) {
		this.isRemote = isRemote;
	}

	public String getStaticUrl() {
		return staticUrl;
	}

	public void setStaticUrl(String staticUrl) {
		this.staticUrl = staticUrl;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }
}
