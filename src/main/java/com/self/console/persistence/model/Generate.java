package com.self.console.persistence.model;

import com.sangame.sow.ssh.orm.mybatis.entity.BaseEntity;

import java.util.ArrayList;
import java.util.Date;

public class Generate extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2425006923352271681L;

	private Long id;

	private Date createdAt;

	private String creator;

	private Date updatedAt;

	private String modifier;

	private Long type;

	private String configKey;

	private String configValue;

	private String configParam;

	private String ftlName;

	private String htmlName;

	private Long disableFlag;

	private String describe;

	// ------------回参----------
	private ArrayList<ConfigArgs> configList;

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getConfigParam() {
		return configParam;
	}

	public void setConfigParam(String configParam) {
		this.configParam = configParam;
	}

	public String getFtlName() {
		return ftlName;
	}

	public void setFtlName(String ftlName) {
		this.ftlName = ftlName;
	}

	public String getHtmlName() {
		return htmlName;
	}

	public void setHtmlName(String htmlName) {
		this.htmlName = htmlName;
	}

	public Long getDisableFlag() {
		return disableFlag;
	}

	public void setDisableFlag(Long disableFlag) {
		this.disableFlag = disableFlag;
	}

	public ArrayList<ConfigArgs> getConfigList() {
		return configList;
	}

	public void setConfigList(ArrayList<ConfigArgs> configList) {
		this.configList = configList;
	}

}