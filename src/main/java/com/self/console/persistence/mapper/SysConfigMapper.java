package com.self.console.persistence.mapper;

import com.sangame.sow.ssh.orm.mybatis.mapper.BaseEntityMapper;
import com.self.console.persistence.model.SysConfig;

public interface SysConfigMapper extends BaseEntityMapper<SysConfig, Long> {
	
	long countUnique(SysConfig sysConfig);

	/**
	 * 根据配置键查询
	 * @param key
	 */
	public SysConfig findByKey(String key);

}
