package com.self.console.persistence.mapper;

import java.util.List;

import com.self.console.persistence.model.Admin;
import org.apache.ibatis.annotations.Param;

import com.sangame.sow.ssh.orm.mybatis.mapper.BaseEntityMapper;

public interface AdminMapper extends BaseEntityMapper<Admin, Long> {

	/**
	 * 查出所有记录
	 * 
	 * @return
	 */
	public List<Admin> findAll();

	/**
	 * 根据角色查询用户
	 * 
	 * @param roleType
	 * @return
	 */
	public List<Admin> findByRole(@Param("p1") long roleType);

	/**
	 * 根据用户名查询
	 * 
	 * @param username
	 * @return
	 */
	public Admin findByUsername(String username);

}
