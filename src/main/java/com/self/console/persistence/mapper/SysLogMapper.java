package com.self.console.persistence.mapper;

import com.sangame.sow.ssh.orm.mybatis.mapper.BaseEntityMapper;
import com.self.console.persistence.model.SysLog;


public interface SysLogMapper extends BaseEntityMapper<SysLog, Long> {

}
