<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<title>模板配置管理</title>
<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
	function formSubmit(){
		if($.trim($('#configKey').val())==""){
		   alert("配置关键字不能为空！");
		   return;
		}
		if($.trim($('#configValue').val())==""){
		   alert("配置值不能为空！");
		   return;
		}
		$("#form1").submit();
	}
		
	function back(){
	   location.href = "${contextPath}/back/template/index.htm";
	}
    $(document).ready(function(){
    <#if config.type?? && config.type=2>
        $(".staticurltr").show();
    </#if>
    });

    function showstatic(){
        var type = $("#type").val();
        if(type == 2){
            $(".staticurltr").show();
        }else{
            $(".staticurltr").hide();
        }
    }
</script>

</head>
<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF">
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr><td height="10"></td></tr>
	<#--常规信息-->
	<table width="100%" border="0" cellspacing="0" id="head1" cellpadding="0" style="border-bottom:1px solid #CCCCCC">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF">
	    <table height="24" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	          <td width="84" height="24" align="center" background="${contextPath}/resource/image/itemnote1.gif">&nbsp;常规信息&nbsp;</td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
      <form name="form1" id='form1' action="${contextPath}/back/template/save.htm" method="post">
          <input name="id" type="hidden" value="${(config.id)!''}"/>
	      <td height="95" align="center" bgcolor="#FFFFFF"> 
	        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="bline" width="15%" align="right">
                        配置关键字：
                    </td>
                    <td class="bline" width="85%" colspan="3">
                        <input style="width:200px" type="text" name="configKey" id="configKey"
                               value="${(config.configKey)!''}" maxlength="32">
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="15%" align="right">
                        配置值：
                    </td>
                    <td class="bline" width="85%" colspan="3">
                        <input style="width:200px" type="text" name="configValue" id="configValue"
                               value="${(config.configValue)!""}" maxlength="500">
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="15%" align="right">
                        类型：
                    </td>
                    <td class="bline" width="85%" colspan="3">
                        <select id="type" name="type" style="width:200px" onchange="showstatic()">
                            <option value="1" <#if config.type?? && config.type=1>selected</#if>>DIV块</option>
                            <option value="2" <#if config.type?? && config.type=2>selected</#if>>静态模块</option>
                        </select>
                    </td>
                </tr>
                <tr class="staticurltr" style="display: none">
                    <td class="bline" width="15%" align="right">
                        生成静态文件名称：
                    </td>
                    <td class="bline" width="85%" colspan="3">
                        <input type="text" style="width:200px" name="staticUrl" id="staticUrl" value="${config.staticUrl!''}">
                        &nbsp;<font color="#a9a9a9">如：/cngold.org/p2p/appfile/</font><font color="red">xxx.html</font>
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="15%" align="right">
                        备注：
                    </td>
                    <td class="bline" width="40%">
                        <textarea rows=3 cols=50 name="remark" id="remark">${(config.remark)!""}</textarea>
                    </td>
                    <td class="bline" width="15%"  align="right">
                        是否根据URL下载内容:
                    </td>
                    <td class="bline" width="30%">
                        <input type="radio" name="isRemote" value="1" <#if !config?? || config?? && config.isRemote?default(1)=1>checked</#if>>是
                        <input type="radio" name="isRemote" value="0" <#if  config?? && config.isRemote?default(1)=0>checked</#if>>否
                    </td>
                </tr>
				<tr>
					<td>
						内容：
					</td>
					<td colspan="3">
                        <textarea rows=30 cols=150 name="configFile" id="configFile">${(config.configFile)!""}</textarea>
					</td>
				</tr>
                <tr>
                    <td width="1%" height="50"></td>
                    <td width="99%" valign="bottom" colspan="3">
                        <input type="button" value="保存" onclick="formSubmit();" style="cursor:pointer;">&#160;&#160;&#160;
                        <input type="button" value="返回列表" onClick="back();" style="cursor:pointer;">
                    </td>
                </tr>
			</table>
	      </td> 
      </form>
    </tr>
  </table>
</body>
</html>