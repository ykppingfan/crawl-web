<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>P2P应用管理台</title>
<link rel="stylesheet" type="text/css" href="${back.cssPath}/base.css">
<link href="${back.cssPath}/frame.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="${back.jsPath}/frame.js" ></script>
<script type="text/javascript" src="${back.jsPath}/main.js"></script>
<link rel="shortcut icon" href="${back.imagePath}/favicon.ico" type="image/x-icon" />
		
<script type="text/javascript">
	function JumpFrame(url1, url2) {
		$('#menufra').get(0).src = url1;
		$('#main').get(0).src = url2;
	}
	
	function zonghe() {
		document.getElementById('zonghe').innerHTML  = " <a href='${contextPath}/back/frame/menu.htm' target='menu' style='color:#F9F900' onclick='zonghe()'>【系统设置】</a>";
	}
</script>
</head>
<body class="showmenu">
	<div class="pagemask"></div>
	<iframe class="iframemask"></iframe>
	<div class="head">
		<div class="top">
			<div class="top_logo">
				<img src="${back.imagePath}/admin_top_logo.gif" width="170" height="37" alt="Mnova Logo" title="Welcome use Mnova" />
			</div>
			<div class="top_link">
				<ul>
					<li class="welcome">${(currentSession.realName)!} 您好！</li>
					<li><a href="${contextPath}/back/frame/welcome.htm" target="main">基本信息</a></li>
					<li><a href="${contextPath}/back/common/logout.htm" target="_top">注销</a></li>
					<li>版本号：<b><a href="http://jira.sangame.com/jira/" target='_blank'>V${config.version?default("4.0.0")} Build.${config.buildNo?default("0")}</a></b></li>
				</ul>
			</div>
		</div>
		<div class="topnav">
			<div class="menuact">
				<div class="moshi" id ='zonghe'> 
					<a href="${contextPath}/back/frame/menu.htm" target="menu" style="color:#FF0000" onclick='zonghe()'>【系统设置】</a>
				</div>
				<div style="color:#FF0000" ></div>
				<ul>
				</ul>
			</div>
		</div>
		<div class="nav" id="nav"></div>
		</div>
	</div>
	<div class="left">
		<div class="menu" id="menu">
			<iframe src="${contextPath}/back/frame/menu.htm" id="menufra" name="menu" frameborder="0"></iframe>
		</div>
	</div>
	<div class="right">
		<div class="main">
			<iframe id="main" name="main" frameborder="0" src="${contextPath}/back/frame/welcome.htm"></iframe>
		</div>
	</div>
</body>
</html>
