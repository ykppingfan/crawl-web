<html>
<head>
<title>menu</title>
<link rel="stylesheet" href="${back.cssPath}/base.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<style>
div {
	padding: 0px;
	margin: 0px;
}

body {
	padding: 0px;
	margin: auto;
	text-align: center;
	background-color: #eff5ed;
	background: url(${back.imagePath}/leftmenu_bg.gif);
	padding-left: 3px;
	overflow: scroll;
	overflow-x: hidden;
}

dl.bitem {
	clear: both;
	width: 140px;
	margin: 0px 0px 5px 12px;
	background: url(${back.imagePath}/menunewbg.gif) repeat-x;
}

dl.bitem dt {
	height: 25px;
	line-height: 25px;
	padding-left: 35px;
	cursor: pointer;
}

dl.bitem dt b {
	color: #4D6C2F;
}

dl.bitem dd {
	padding: 3px 3px 3px 3px;
	background-color: #fff;
}

div.items {
	clear: both;
	padding: 0px;
	height: 0px;
}

.fllct {
	float: left;
	width: 85px;
}

.flrct {
	padding-top: 3px;
	float: left;
}

.sitemu li {
	padding: 0px 0px 0px 18px;
	line-height: 22px;
	background: url(${back.imagePath}/arr4.gif) no-repeat 5px 9px;
}

ul {
	padding-top: 3px;
}

li {
	height: 24px;
}

a.mmac div {
	background: url(${back.imagePath}/leftbg2.gif) no-repeat;
	height: 37px !important;
	height: 47px;
	padding: 6px 4px 4px 10px;
	word-wrap: break-word;
	word-break: break-all;
	font-weight: bold;
	color: #325304;
}

a.mm div {
	background: url(${back.imagePath}/leftmbg1.gif) no-repeat;
	height: 37px !important;
	height: 47px;
	padding: 6px 4px 4px 10px;
	word-wrap: break-word;
	word-break: break-all;
	font-weight: bold;
	color: #475645;
	cursor: pointer;
}

a.mm:hover div {
	background: url(${back.imagePath}/leftbg2.gif) no-repeat;
	color: #4F7632;
}

.mmf {
	height: 1px;
	padding: 5px 7px 5px 7px;
}

#mainct {
	padding-top: 8px;
	background: url(${back.imagePath}/idnbg1.gif) repeat-y;
}
</style>
<base target="main" />

<script type="text/javascript">
	function ShowMainMenu(n) {
		var ids = [1, 100];
		for(var i = 0; i < ids.length; i++) {
			$('#ct'+ids[i])[0].style.display = 'none';
			$('#link'+ids[i])[0].className = 'mm';
		}

		$('#ct'+n)[0].style.display = 'block';
		$('#link'+n)[0].className = 'mmac';
	}
	
	function isShow(obj) {
		if(obj.style.display == 'NONE' || obj.style.display == 'none') {
			return false;
		} else {
			return true;
		}
	}
	
	$(document).ready(function() {
		$('.bitem').click(function() {
			var next = $(this).find('dd').eq(0);
		
			if(isShow(next[0])) {
				next.hide();
			} else {
				next.show();
			}
		})
		$('.sitemu').click(function(event) {
			event.stopPropagation();
		});
	});
</script>
</head>
<body target="main">
	<table width="180" align="left" border='0' cellspacing='0' cellpadding='0'>
		<tr>
			<td valign='top' style='padding-top: 10px' width='20'>
				<a id='link1' class='mmac'>
					<div onClick="ShowMainMenu(1)">核心</div>
				</a>
                <@back.hasOper operCode='99_1_0'>
                <a id='link100' class='mm'>
                	<div onClick="ShowMainMenu(100)">系统</div>
                </a>
                </@back.hasOper>
				<div class='mmf'></div>
			</td>
			<td width='160' id='mainct' valign="top">
				<div id='ct1'>
					<#include "menu/core.ftl">
				</div>
                <@back.hasOper operCode='99_1_0'>
				<div id='ct100' style="display: none;">
					<#include "menu/system.ftl">
				</div>
                </@back.hasOper>
			</td>
		</tr>
		<tr>
			<td width='26'></td>
			<td width='160' valign='top'><img src='${back.imagePath}/idnbgfoot.gif' /></td>
		</tr>
	</table>
</body>
</html>