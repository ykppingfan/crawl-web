<@back.hasOper operCode='99_1_0'>
<dl class="bitem">
	<dt onClick="showHide(&quot;items3_100&quot;)">
		<b>用户管理</b>
	</dt>
	<dd id="items3_100" class="sitem" style="display: block;">
		<ul class="sitemu">
			<li>
				<@back.hasOper operCode='99_1_1'>
				<div class='items'>
					<div class='fllct'>
						<a href='${contextPath}/back/system/admin/index.htm' target='main'>系统管理员</a>
					</div>
				</@back.hasOper>
				<@back.hasOper operCode='99_1_2'>
					<div class='flrct'>
						<a href='${contextPath}/back/system/admin/add.htm' target='main'> <img src='${back.imagePath}/gtk-sadd.png' alt='添加管理员' title='添加管理员' /></a>
					</div>
				</div>
				</@back.hasOper>
			</li>
            <@back.hasOper operCode='99_3_1'>
			    <li>
				<div class='items'>
					<div class='fllct'>
						<a href='${contextPath}/back/system/config/index.htm' target='main'>系统配置管理</a>
					</div>
				<@back.hasOper operCode='99_3_2'>
					<div class='flrct'>
						<a href='${contextPath}/back/system/config/add.htm' target='main'> <img src='${back.imagePath}/gtk-sadd.png' alt='添加配置项' title='添加配置项' /></a>
					</div>

				</@back.hasOper>
                </div>
			    </li>
            </@back.hasOper>
			<li>
				<@back.hasOper operCode='99_4_1'>
				<div class='items'>
					<div class='fllct'>
						<a href='${contextPath}/back/system/log/index.htm' target='main'>系统日志管理</a>
					</div>
				</div>
				</@back.hasOper>
			</li>
		</ul>
	</dd>
</dl>
</@back.hasOper>
<!-- Item 3 End -->
