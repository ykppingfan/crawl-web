<dl class='bitem'>
    <dt>
        <b>内容管理</b>
    </dt>
    <dd style='display: block' class='sitem' id='items1_1'>
        <ul class='sitemu'>
            <li>
                <div class='items'>
                <@back.hasOper operCode='2_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/platform/index.htm' target='main'>机构管理</a>
                    </div>
                     <div class='flrct'>
						<a href='${contextPath}/back/p2p/platform/add.htm' target='main'><img src='${back.imagePath}/gtk-sadd.png' alt='添加机构' title='添加机构' /></a>
					</div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='2_2_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/label/index.htm' target='main'>标签管理</a>
                    </div>
                    <div class='flrct'>
						<a href='${contextPath}/back/p2p/label/add.htm' target='main'><img src='${back.imagePath}/gtk-sadd.png' alt='添加标签' title='添加标签' /></a>
					</div>
                </@back.hasOper>
                </div>
            </li>
        	<li>
                <div class='items'>
                <@back.hasOper operCode='3_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/product/index.htm' target='main'>产品管理</a>
                    </div>
                    <div class='flrct'>
						<a href='${contextPath}/back/p2p/product/add.htm' target='main'><img src='${back.imagePath}/gtk-sadd.png' alt='添加产品' title='添加产品' /></a>
					</div>
                </@back.hasOper>
                </div>
            </li>
            
            <li>
                <div class='items'>
                <@back.hasOper operCode='3_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/buylog/index.htm' target='main'>产品抢购日志</a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
        	<li>
                <div class='items'>
                <@back.hasOper operCode='2_3_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/ranking/index.htm' target='main'>排名管理</a>
                    </div>
                    <div class='flrct'>
						<a href='${contextPath}/back/p2p/ranking/add.htm' target='main'><img src='${back.imagePath}/gtk-sadd.png' alt='平台排名' title='平台排名' /></a>
					</div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='2_4_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/quotation/index.htm' target='main'>数据走势</a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='2_4_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/percent/index.htm' target='main'>借款期限、金额</a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='4_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/template/index.htm' target='main' style='width:100px'>模版配置管理</a>
                    </div>
                </@back.hasOper>
                <@back.hasOper operCode='4_1_2'>
                    <div class='flrct'>
                        <a href='${contextPath}/back/template/add.htm' target='main'> <img src='${back.imagePath}/gtk-sadd.png' alt='添加模板' title='添加模板' /></a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='4_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/gen/index.htm' target='main' style='width:100px'>静态模版生成</a>
                    </div>
                </@back.hasOper>
                <@back.hasOper operCode='4_1_2'>
                    <div class='flrct'>
                        <a href='${contextPath}/back/gen/add.htm' target='main'> <img src='${back.imagePath}/gtk-sadd.png' alt='添加模板' title='添加模板' /></a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
            <li>
                <div class='items'>
                <@back.hasOper operCode='1_1_1'>
                    <div class='fllct'>
                        <a href='${contextPath}/back/p2p/region/index.htm' target='main'>地区管理</a>
                    </div>
                </@back.hasOper>
                </div>
            </li>
        </ul>
    </dd>
</dl>

