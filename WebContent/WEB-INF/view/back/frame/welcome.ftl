<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'>
		<link href="${back.cssPath}/indexbody.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="${back.jsPath}/main.js"></script>
	</head>
	<body leftmargin="8" topmargin='8' background='${contextPath}/resource/image/allbg.gif'>
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div style='float:left;font-size:14px;font-wight:bold;'>
						<h1>欢迎使用数游内容管理系统... </h1>
					</div>
					<div id='' style='float:right;padding-right:8px;'>
						<!--  //保留位置（顶右）  -->
					</div>
				</td>
			</tr>
			<tr>
				<td height="1" background="${contextPath}/resource/image/sp_bg.gif" style='padding:0px'></td>
			</tr>
		</table>
		<div id='mainmsg'>
			<div id="leftside">
				<dl class='dbox'>
					<dt class='lside'>
						<div class='l'>>>用户登录信息</div>
					</dt>
					<dd class='intable'>
						<table width="98%" class="dboxtable">
							<tr>
								<td colspan='2' class='nline'>您的级别：<font color='red'>
									<#if currentSession ?? && currentSession.roleFlag ?? && currentSession.roleFlag == -1>超级管理员
									<#elseif currentSession ?? && currentSession.roleFlag ?? && currentSession.roleFlag == 1>普通管理员
									</#if></font>
								</td>
							</tr>
							<tr>
								<td class='nline'>本次登录IP：<font color='red'>${(currentSession.loginIp)!''}</font></td>
								<td class='nline'>本次登录时间：<font color='red'>${(currentSession.loginTime?datetime)!''}</font></td>
							</tr>
							<tr>
								<td class='nline'>上次登录IP：<font color='red'>${(currentSession.lastLoginIp)!''}</font></td>
								<td class='nline'>上次登录时间：<font color='red'>${(currentSession.lastLoginTime?datetime)!''}</font></td>
							</tr>
						</table>
					</dd>
				</dl>
			</div>
		</div>
		<br style='clear:both'/>
		<!-- //底部 -->
		<div align="center" class="footer">
			Copyright &copy; <a href='http://www.sangame.com' target='_blank'><u>P2P-ADMIN</u></a>. 数游科技 版权所有
		</div>
	</body>
</html>