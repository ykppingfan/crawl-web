<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系统日志管理</title>
<link href="${contextPath}/resource/css/base.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="${back.jsPath}/My97DatePicker/WdatePicker.js"></script>

<script type="text/javascript">
	function jumpPage(page) {
		$("#form3").attr("action", "${contextPath}/back/system/log/index.htm?currentPage=" + page);
		$("#form3").submit();
	}

	function selAll() {
		var arcIDs = document.getElementsByName("arcID");
		for (i = 0; i < arcIDs.length; i++) {
			if (!arcIDs[i].checked) {
				arcIDs[i].checked = true;
			}
		}
	}

	function noSelAll() {
		var arcIDs = document.getElementsByName("arcID");
		for (i = 0; i < arcIDs.length; i++) {
			if (arcIDs[i].checked) {
				arcIDs[i].checked = false;
			}
		}
	}

	function getCheckboxItem() {
		var allSel = "";
		var arcIDs = document.getElementsByName("arcID");
		for (i = 0; i < arcIDs.length; i++) {
			if (arcIDs[i].checked) {
				if (allSel == "")
					allSel = arcIDs[i].value;
				else
					allSel = allSel + "," + arcIDs[i].value;
			}
		}
		return allSel;
	}

	function show(id) {
		location.href = "${contextPath}/back/system/log/show.htm?id=" + id;
	}

	function destory(id) {
		if (confirm("确定要删除数据吗？")) {
			location.href = "${contextPath}/back/system/log/destory.htm?ids=" + id;
		}
	}

	function deleteAll() {
		var ids = getCheckboxItem();
		if (ids == "") {
			alert("请选择要删除的数据！");
		} else {
			if (confirm("确定要删除数据吗？")) {
				location.href = "${contextPath}/back/system/log/destory.htm?ids=" + ids;
			}
		}
	}

</script>
<style>
ul {
	padding: 0px;
	margin: 0px;
}

li {
	float: left;
	padding-right: 8px;
	line-height: 24px;
}
</style>
</head>
<body background='${contextPath}/resource/image/allbg.gif' leftmargin='8' topmargin='8'>
  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#D1DDAA" style="margin-bottom: 6px">
    <tr>
      <td background="${back.imagePath}/wbg.gif">
        <form action='${contextPath}/back/system/log/index.htm' name='form3' id='form3' method='post'>
          <ul>
            <li>关键字：<input type="text" id="keyword" name="operDesc" style="width: 100px" value="${(search.operDesc)!''}" /></li>
            <li>操作人： 
	            <select name="userId" id="userId" style="width: 100px">
	                <option value=''>--请选择--</option> 
	                <#list users as user>
	                <option value='${user.id}'<#if search?? && search.userId?? && search.userId = user.id>selected</#if>>${user.userName}</option> 
	                </#list>
	            </select>
            </li>
            <li>操作类型： 
	            <select name="operType" id="operType" style="width: 100px">
	                <option value=''>--请选择--</option> 
	                <#list operTypes as type>
	                <option value='${type.value}'<#if search?? && search.operType?? && search.operType = type.getValue()>selected</#if>>${type.getName()}</option> 
	                </#list>
	            </select>
            </li>
            <li>IP地址：<input name="operIp" type="text" id="cip" size="15" style="width: 100px" value="${(search.operIp)!''}" /></li>
            <li>时间： 
            	<input name="queryBeginTime" style="width: 80px" class="date-input" readonly="true" onclick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd'})" type="text" id="queryBeginTime" value="${(search.queryBeginTime?string(" yyyy-MM-dd HH:mm:ss"))!''}"/>
            	至 
            	<input name="queryEndTime" style="width: 80px" class="date-input" id="queryEndTime" readonly="true" onclick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd'})" type="text" value="${(search.queryEndTime?string(" yyyy-MM-dd HH:mm:ss"))!''}"/>
            </li>
            <li>显示记录数:</li>
            <li><select id="pageSize" name="pageSize">
                <option value='30'<#if search.pageSize=30>selected</#if>>30</option>
                <option value='50'<#if search.pageSize=50>selected</#if>>50</option>
                <option value='100'<#if search.pageSize=100>selected</#if>>100</option>
                <option value='200'<#if search.pageSize=200>selected</#if>>200</option>
            </select></li> &nbsp;
            <li><input id='sousuo' type="submit" value='搜  索' width="60" height="22" border="0" class="np" /></li>
            <ul>
        </form>
      </td>
  </table>

  <table width="98%" border="0" cellpadding="3" cellspacing="1" bgcolor="#D1DDAA" align="center">
    <form name="form2" id='form2' method="post">
      <input type="hidden" name="dopost" value="save">
      <tr>
        <td height="28" colspan="8" background='${contextPath}/resource/image/tbg.gif'>
          <table width="96%" border="0" cellspacing="1" cellpadding="1">
            <tr>
              <td width="24%"><b>系统日志列表管理</b></td>
              <td width="76%" align="right"></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr bgcolor="#FDFEE9" align="center">
        <td width="5%" height="24">选择</td>
        <td width="5%">ID</td>
        <td width="8%">操作人</td>
        <td width="8%">操作类型</td>
        <td width="12%">操作时间</td>
        <td width="8%">操作IP</td>
        <td width="46%">操作描述</td>
        <td width="8%">操作</td>
      </tr>
      <#if page?? && page.result?size gt 0 > 
      <#list page.result as pr>
      <tr align="center" bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='#f4fbf4';" onMouseOut="javascript:this.bgColor='#FFFFFF';">
        <td height="24"><input type="checkBox" value="${pr.id?if_exists}" name="arcID" id="arcID"></td>
        <td>${pr.id?if_exists}</td>
        <td>${pr.creator?if_exists}</td>
        <td>
        <#list operTypes as type>
           <#if pr.operType?? && pr.operType = type.getValue()>
           	 ${type.getName()}
           </#if>
        </#list>
        </td>
        <td>${(pr.createdAt?string("yyyy-MM-dd HH:mm:ss"))!''}</td>
        <td>${pr.operIp?if_exists}</td>
        <td align="left">
        	<#if pr.operDesc?? && pr.operDesc?length &gt; 50> 
        		${pr.operDesc[0..50]?if_exists}... 
        	<#else> 
        		${pr.operDesc?if_exists} 
        	</#if>
        </td>
        <td><a href="javascript:show(${(pr.id)!})">查看</a>&nbsp; <a href="javascript:destory(${(pr.id)!})">删除</a></td>
      </tr>
      </#list> 
      </#if>
    </form>
    <tr bgcolor="#FAFAF1">
      <td height="36" colspan="12">&nbsp; <a href="javascript:selAll()" class="coolbg">全选</a> <a href="javascript:noSelAll()" class="coolbg">取消</a> <a href="javascript:deleteAll()" class="coolbg">&nbsp;删除&nbsp;</a>
      </td>
    </tr>
    <tr>
      <td height='20' align='center' bgcolor='#EEF4EA' colspan="8"><@back.pagination page = page jumpMehtod=selectData /></td>
    </tr>

    <tr>
      <td height="36" bgcolor="#FFFFFF" align="center" colspan="8">页面执行时间:${action.executeTime?default(0)}ms</td>
    </tr>
  </table>
</body>
</html>
