<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<title>操作日志-查看</title>
<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="${back.jsPath}/scriptvalidator.js"></script>
<script type="text/javascript">
	
</script>
</head>
<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF">
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#111111" style="BORDER-COLLAPSE: collapse">
    <tr>
      <td width="100%" height="20" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="30"><IMG height=14 src="${contextPath}/resource/image/book1.gif" width=20> &nbsp;<a href="${contextPath}/back/system/log/index.htm"><u>操作日志管理</u></a>&gt;&gt;查看</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%" height="1" background="${contextPath}/resource/image/sp_bg.gif"></td>
    </tr>
  </table>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td height="215" align="center" valign="top" bgcolor="#FFFFFF">
        <table width="98%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td width="16%" height="30">操作人：</td>
            <td width="84%">${sysLog?if_exists.creator?if_exists}&nbsp;</td>
          </tr>
          <tr>
            <td height="30">操作类型：</td>
            <td>
            <#list operTypes as type>
	           <#if sysLog.operType?? && sysLog.operType = type.getValue()>
	           	 ${type.getName()}
	           </#if>
	        </#list>
            </td>
          </tr>
          <tr>
            <td height="30">操作时间：</td>
            <td>${(sysLog.createdAt?string("yyyy-MM-dd HH:mm:ss"))!''}&nbsp;</td>
          </tr>
          <tr>
            <td width="16%" height="30">操作IP：</td>
            <td width="84%">${sysLog?if_exists.operIp?if_exists}&nbsp;</td>
          </tr>
          <tr>
            <td height="30">操作描述：</td>
            <td>${sysLog?if_exists.operDesc?if_exists}&nbsp;</td>
          </tr>
          <tr>
            <td height="60">&nbsp;</td>
            <td><input type="button" value=" 返回 " onclick="location.href='index.htm'" class="coolbg np" /></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>