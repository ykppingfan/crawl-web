<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<title>用户-查看</title>
		<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="${back.jsPath}/scriptvalidator.js"></script>
	</head>
	<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF"> 
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#111111" style="BORDER-COLLAPSE: collapse"> 
		  <tr> 
		    <td width="100%" height="20" valign="top"> 
		    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
		        <tr> 
		          <td height="30"><IMG height=14 src="${contextPath}/resource/image/book1.gif" width=20> &nbsp;<a href="${contextPath}/back/system/admin/index.htm"><u> 系统管理员 </u></a>&gt;&gt;查看</td> 
		        </tr> 
		      </table></td> 
		  </tr> 
		  <tr> 
		    <td width="100%" height="1" background="${contextPath}/resource/image/sp_bg.gif"></td> 
		  </tr> 
		</table> 
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"> 
			<tr>
				<td height="215" align="center" valign="top" bgcolor="#FFFFFF">
					<table width="98%" border="0" cellspacing="1" cellpadding="1">
						<tr>
							<td width="16%" height="30">
								用户名：
							</td>
							<td width="84%">
								${admin?if_exists.userName?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								创建时间：
							</td>
							<td>
								${(admin.createdAt?string("yyyy-MM-dd HH:mm:ss"))!''}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								更新时间：
							</td>
							<td>
								${(admin.updatedAt?string("yyyy-MM-dd HH:mm:ss"))!''}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								角色：
							</td>
							<td>
							<#if admin.roleId??>
								<#if admin.roleId == -1>超级管理员<#elseif admin.roleId == 1>管理员<#else>普通用户</#if>&nbsp;
							</#if>
							</td>
						</tr>
						<tr>
							<td height="30">
								是否已授权：
							</td>
							<td>
								<#if admin?? && (admin.loginKey)??>
									是&nbsp;&nbsp;&nbsp;<a href="${contextPath}/back/system/admin/authCode.htm?id=${admin.id}" target="_blank">查看二维码</a>
								<#else>
									否
								</#if>
							</td>
						</tr>
						<tr>
							<td height="30">
								登录失败次数：
							</td>
							<td>
								${admin?if_exists.failNumber?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								最后登录IP：
							</td>
							<td>
								${admin?if_exists.lastLoginIp?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								最后登录时间：
							</td>
							<td>
								${(admin.lastLoginTime?string("yyyy-MM-dd HH:mm:ss"))!''}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								创建人：
							</td>
							<td>
								${admin?if_exists.creator?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								更新人：
							</td>
							<td>
								${admin?if_exists.modifier?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="30">
								备注：
							</td>
							<td>
								${admin?if_exists.remark?if_exists}&nbsp;
							</td>
						</tr>
						<tr>
							<td height="60">
								&nbsp;
							</td>
							<td>
								<input type="button" value=" 返回 " onclick="location.href='index.htm'" class="coolbg np" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>