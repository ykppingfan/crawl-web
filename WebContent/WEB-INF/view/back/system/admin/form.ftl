<script type="text/javascript">
	var flag=true;
	function formSubmit() {
		var userName=$.trim($('#userName').val());
		if (userName=="") {
			$('#userNameMsg').css('color', 'red');	
			$('#userNameMsg').html("用户名不能为空！");
			$('#userName').focus();
			return false;
		}
	     
		if (!flag) {
			return false;
		}
		document.getElementById("roleId").disabled=false; 
		$("#form1").submit();
	}
	
	function checkLoginNameUnique() {
	 	var url = '${contextPath}/back/system/admin/ajaxCheckUnique.htm';
	 	var userName = $.trim($('#userName').val());
	 	var preUserName = $.trim($('#preUserName').val());
	 	if (userName == preUserName) {
	 		return;
	 	}
		if (userName != null && userName != "") {
			$.post(url, {'userName': userName, 'id': ${(admin.id)!'0'}}, function(jsonObj) {
				if(jsonObj.timeout) {
					location.href="index.htm";
				} else if(jsonObj.flag) {
					flag=true;
					$('#userNameMsg').css('color', 'green');
					$('#userNameMsg').html(jsonObj.msg);
				} else {
					flag=false;
					$('#userNameMsg').css('color', 'red');	
					$('#userNameMsg').html(jsonObj.msg);
				}
			}, 'json');
		}
	}
</script>
	<td height="95" align="center" bgcolor="#FFFFFF"> 
		<table width="98%" border="0"> 
			<tr>
				<td	class="bline" width="15%" align="right">
					用户名：
				</td>
				<td class="bline" width="85%">
					<input type="hidden" id="preUserName" name="preUserName" value="${(admin.userName)!''}">
					<input style="width:200px" type="text" name="userName" id="userName" value="${(admin.userName)!''}" onblur="checkLoginNameUnique()">
					&nbsp;<span id="userNameMsg"></span>
					<input type='text' style='display:none'/>
				</td>
			</tr>
			<tr>
				<td	class="bline" width="15%" align="right">
					角色：
				</td>
				<td class="bline" width="85%">
					<select id="roleId" name="roleId">
					<#if roles??>
						<#list roles as role>
						<option value="${role.getValue()}" <#if admin?? && admin.roleId?? && role.getValue() == admin.roleId>selected</#if>>${role.getName()}</option>
						</#list>
					</#if>
					</select>
				</td>
			</tr>
			<tr>
				<td height="30" class="bline">
					是否被禁用：
				</td>
				<td class="bline">
				<#if admin?exists && admin.disableFlag?exists>
					<input name="disableFlag" type="radio" class="np" value="0" <#if admin?exists && admin.disableFlag?exists && admin.disableFlag==0>checked="checked"</#if>/>否
					<input name="disableFlag" type="radio" class="np" value="1" <#if admin?exists && admin.disableFlag?exists && admin.disableFlag==1>checked="checked"</#if>/>是
				<#else>
					<input name="disableFlag" type="radio" class="np" value="0" checked="checked"/>否
					<input name="disableFlag" type="radio" class="np" value="1" />是
				</#if>
				</td>
			</tr>
			<tr>
				<td	class="bline" width="15%" align="right">
					备注：
				</td>
				<td class="bline" width="85%">
					<textarea  rows=3 cols=50 name="remark" id="remark">${(admin.remark )!""}</textarea>
				</td>
			</tr>
			<tr>
				<td width="1%" height="50"></td>
				<td width="99%" valign="bottom">
					<img src="${contextPath}/resource/image/button_ok.gif" onclick="formSubmit();" style="cursor:pointer" />
					&nbsp;&nbsp;&nbsp;
					<img src="${contextPath}/resource/image/button_back.gif" onclick="javascript:location='${contextPath}/back/system/admin/index.htm'" width="60" height="22" border="0" />
				</td>
			</tr> 
		</table>
	</td> 