<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系统管理员管理</title>
<link href="${contextPath}/resource/css/base.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="${back.jsPath}/My97DatePicker/WdatePicker.js"></script>
 
<script  type="text/javascript">
	function jumpPage(page) {
		$("#form3").attr("action","${contextPath}/back/system/admin/index.htm?currentPage="+page); 
		$("#form3").submit();	
	}
	
	
	function selAll() {
		var arcIDs = document.getElementsByName("arcID");
		for( i = 0; i < arcIDs.length; i++ ) {
			if( !arcIDs[i].checked ) {
				arcIDs[i].checked = true;
			}
		}
	}
	
	function noSelAll() {
		var arcIDs = document.getElementsByName( "arcID" );
		for ( i = 0; i < arcIDs.length; i++ )
		{
			if ( arcIDs[i].checked )
			{
				arcIDs[i].checked=false;
			}
		}
	}

	function getCheckboxItem() {
		var allSel = "";
		var arcIDs = document.getElementsByName("arcID");
		for( i = 0; i < arcIDs.length; i++ )
		{
			if( arcIDs[i].checked )
			{
				if( allSel == "" )
					allSel = arcIDs[i].value;
				else
					allSel = allSel + "," + arcIDs[i].value;
			}
		}
		return allSel;
	}
	
	function deleteData(id) {
		if( confirm( "确定要删除吗？" ) ){
			location.href = "${contextPath}/back/system/admin/destory.htm?ids=" + id;
		}
	}
	
	function deleteAll() {
		var ids = getCheckboxItem();
		if( ids == "" ){
			alert( "请选择要删除的数据！" );
		}else{
			if( confirm( "确定要删除数据吗？" ) ){
				location.href = "${contextPath}/back/system/admin/destory.htm?ids=" + ids;
			}
		}
	}
	
	function formSubmit() {
		document.form3.submit();
	}
</script>
 <style> 
ul { padding:0px; margin:0px;}
li { float:left; padding-right:8px; line-height:24px; }
</style>
</head>
	<body background='${contextPath}/resource/image/allbg.gif' leftmargin='8' topmargin='8'>
		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#D1DDAA" style="margin-bottom:6px">
			<tr>
				<td background="${back.imagePath}/wbg.gif" colspan="6">
					<form action='${contextPath}/back/system/admin/index.htm' name='form3' id='form3' method='post'>
						<ul>
							<li> 用户名：</li>
							<li> <input type='text' name='userName' value='${(search.userName)!''}' style='width:100px' /></li>
							<li> 最后登录IP:</li><li> <input type="text" name="lastLoginIp" id="lastLoginIp" value="${(search.lastLoginIp)!''}"/> </li>
							<li> 最后登录时间：<input name="lastLoginTime" class="date-input" readonly="true" onclick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd'})" type="text" id="lastLoginTime" value="${(search.lastLoginTime?string("yyyy-MM-dd HH:mm:ss"))!''}"/></li>
							<li> 显示记录数:</li>
							<li><select id="pageSize" name="pageSize">
								<option value='30' <#if search.pageSize=30>selected</#if>>30</option>
								<option value='50' <#if search.pageSize=50>selected</#if>>50</option>
								<option value='100' <#if search.pageSize=100>selected</#if>>100</option>
								<option value='200' <#if search.pageSize=200>selected</#if>>200</option>
								</select>
							</li>
				          	&nbsp;
							<li><input id='sousuo' type="submit" value='搜  索' width="60" height="22" border="0" class="np" /></li>
						<ul>	
					</form>
				</td>
			</tr>
		</table>
	
		<table width="98%" border="0" cellpadding="3" cellspacing="1" bgcolor="#D1DDAA" align="center">
			<form name="form2" id='form2' method="post">
				<input type="hidden" name="dopost" value="save">
				<tr> 
					<td height="28" colspan="12" background='${contextPath}/resource/image/tbg.gif'>
						<table width="96%" border="0" cellspacing="1" cellpadding="1">
							<tr> 
								<td width="24%"><b>管理员列表管理</b> </td>
								<td width="76%" align="right"><b>
									<a href="${contextPath}/back/system/admin/add.htm"><u>增加管理员</u></a>&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr bgcolor="#FDFEE9" align="center" > 
					<td width="5%" height="24">选择</td>
					<td width="3%">ID</td>
					<td width="8%">用户名</td>
					<td width="8%">角色</td>
					<td width="8%">创建人</td>
					<td width="8%">更新人</td>
					<td width="8%">登录失败次数</td>
					<td width="8%">最后登录IP</td>
					<td width="12%">最后登录时间</td>
					<td width="5%">是否禁用</td>
					<td width="15%">备注</td>
					<td width="12%">操作</td>
				</tr>
		<#if page?? && page.result?size gt 0 > 
			<#list page.result as pr>
				<tr align="center" bgcolor="#FFFFFF"  onMouseMove="javascript:this.bgColor='#f4fbf4';" onMouseOut="javascript:this.bgColor='#FFFFFF';"> 
					<td height="24">
						<input type="checkBox" value="${pr.id?if_exists}" 
							<#if currentSession?? && currentSession.roleId?? && currentSession.roleId == -1> name="arcID" id="arcID" <#else>disabled</#if>>
					</td>
					<td>${pr.id?if_exists}</td>
					<td>
						<#if  pr.userName?length &gt; 15>
							${pr.userName[0..10]?if_exists}...
						<#else>
							${pr.userName?if_exists}
						</#if>
					</td>
					<td>
						<#if roles??>
							<#list roles as role>
								<#if pr?? && pr.roleId?? && role.getValue() == pr.roleId>
									${role.getName()}
								</#if>
							</#list>
						</#if>
					</td>
					<td>${pr.creator?if_exists}</td>
					<td>${pr.modifier?if_exists}</td>
					<td>${pr.failNumber?if_exists}</td>
					<td>${pr.lastLoginIp?if_exists}</td>
					<td>${(pr.lastLoginTime?string("yyyy-MM-dd HH:mm:ss"))!''}</td>
					<td>
						<#if pr.disableFlag?exists && pr.disableFlag==1>
							<font color="red">是</font>
						<#else>
							否
						</#if>
					</td>
					<td>
						<#if pr.remark??>
							<#if pr.remark?length &gt; 15>
								${pr.remark[0..10]?if_exists}...
							<#else>
								${pr.remark?if_exists}
							</#if>
						</#if>
					</td>
					<td>
						<a href="${contextPath}/back/system/admin/show.htm?id=${(pr.id)!}">查看</a>
						<#if currentSession?? && currentSession.roleId?? && currentSession.roleId == -1>
							| <a href="${contextPath}/back/system/admin/edit.htm?id=${(pr.id)!}">修改</a>
							| <a href="javascript:deleteData(${(pr.id)!})">删除</a>
						</#if>
					</td>
				</tr>
			</#list>
		</#if>	
			</form>
		
				<tr bgcolor="#FAFAF1">
					<td height="36" colspan="12">
						&nbsp;
						<a href="javascript:selAll()" class="coolbg">全选</a>
						<a href="javascript:noSelAll()" class="coolbg">取消</a> 
						<a href="javascript:deleteAll()" class="coolbg">&nbsp;删除&nbsp;</a> 
					</td>
				</tr>
				<tr> 
					<td height='20' align='center' bgcolor='#EEF4EA' colspan="12">
						<@back.pagination page = page jumpMehtod=selectData /> 
					</td>
				</tr>
		  
				<tr>
					<td height="36" bgcolor="#FFFFFF" align="center"  colspan="12">
						页面执行时间:${action.executeTime?default(0)}ms
					</td>
				</tr>
		</table>
	</body>

</html>
