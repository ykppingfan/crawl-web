<html> 
<head> 
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'> 
<title>修改</title> 
<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'> 
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript"> 
// 生成授权码
function generateAuthCode(id){
	if(id != ""){
		$.ajax({
			url:'${contextPath}/back/system/admin/generateAuthCode.htm',
			type:'post',
			dataType:'html',
			data:{"id":id},
			success:function(data){
				var jsonObj = eval("(" + data + ")");
				if(jsonObj.flag){
					var str = "是";
					str += "<input type='button' value='更改' class='coolbg np' onclick=generateAuthCode("+jsonObj.data+") />";
					str += "&nbsp;&nbsp;&nbsp;<a href='${contextPath}/back/system/admin/authCode.htm?id=" + id + "' target='_blank'>查看二维码</a>";
					$("#generateAuthCodeId").html("");
					$("#generateAuthCodeId").html(str);
				} else {
					$("#generateAuthCodeId").html(jsonObj.msg);
				}
			},
			error:function(data){
				alert("生成授权码异常！");
			}
		});
	}
}
</script> 
</head> 
<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#111111" style="BORDER-COLLAPSE: collapse"> 
  <tr> 
    <td width="100%" height="20" valign="top"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
        <tr> 
          <td height="30"><IMG height=14 src="${contextPath}/resource/image/book1.gif" width=20> &nbsp;<a href="${contextPath}/back/system/admin/index.htm"><u> 系统管理员 </u></a>&gt;&gt;修改</td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr> 
    <td width="100%" height="1" background="${contextPath}/resource/image/sp_bg.gif"></td> 
  </tr> 

</table> 
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr><td height="10"></td></tr>
  <tr> 
  	<form name="form1" id='form1' action="${contextPath}/back/system/admin/update.htm?id=${admin.id}" method="post"> 
		<#include "form.ftl">
  	</form>
  </tr> 

</table> 
</body> 
</html>