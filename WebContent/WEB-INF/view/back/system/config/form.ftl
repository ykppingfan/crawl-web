<script type="text/javascript">
		var flag=true;
		<#--验证长度-->
		function validLength(id){
			var name = $.trim($("#"+id).val());
			var bytes = 0;
		    for(var i = 0; i < name.length; i++){
			    if(name.charCodeAt(i) > 255){
			    	bytes += 2;
			    }else{
					bytes++;
			    }
			}
			$("#"+id).val(name);
			if(bytes > 30 || bytes < 4){
				$("#"+id+"_msg").css('color', 'red');	
				$("#"+id+"_msg").html("长度限制在[4-30]字节之间!");
			 	return false;
		 	}else{
		 		$("#"+id+"_msg").html("");
			 	return true;
		 	}
		}
		
		function checkNameUnique() {
		 	var url = '${contextPath}/back/system/config/ajaxCheckNameUnique.htm';
			var configKey = $.trim($("#configKey").val());
			$("#configKey").val(configKey);
			
			if(configKey != null && configKey != ""){
				$.post(url, {
					'configKey': configKey,
					'id':${(sysConfig.id)?default(0)}
					}, function(jsonObj) {
						if(jsonObj.timeout) {
							location.href="index.htm";
						} else if(jsonObj.flag) {
							flag=true;
							$('#configKey_msg').css('color', 'green');
							$('#configKey_msg').html(jsonObj.msg);
						} else {
							flag=false;
							$('#configKey_msg').css('color', 'red');	
							$('#configKey_msg').html(jsonObj.msg);
						}
				}, 'json');
			}
		}
		
		function formSubmit(){
			var showName=$.trim($('#showName').val());
			if(showName==""){
				$('#showName_msg').css('color', 'red');	
				$('#showName_msg').html("名称不能为空！");
			     $('#showName').focus();
			     return false;
		     } else {
		     	if(!validLength("showName")){
					$('#showName').focus();
					return;
				}
		     }
		     
		    var configKey=$.trim($('#configKey').val());
			if(configKey==""){
				$('#configKey_msg').css('color', 'red');	
				$('#configKey_msg').html("key不能为空！");
				$('#configKey').focus();
			     return false;
		     }
		     
		     if(!flag){
				return;
			 }
			$("#form1").submit();
		}
</script>
<td height="95" align="center" bgcolor="#FFFFFF"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
					<td	class="bline" width="15%" align="right">
						配置名称：
					</td>
					<td class="bline" width="90%">
						<input style="width:200px" type="text"  name="showName" id="showName" value="${(sysConfig.showName)!''}">
						&nbsp;<span id="showName_msg"></span>
					</td>
				 </tr> 
		 	  	 <tr>
					<td	class="bline" width="15%" align="right">
						配置关键字：
					</td>
					<td class="bline" width="85%">
						<input style="width:200px" type="text"  name="configKey" id="configKey" value="${(sysConfig.configKey)!''}" onBlur="checkNameUnique()">
						&nbsp;<span id="configKey_msg"></span>
					</td>
				 </tr>
		 	  	 <tr>
					<td	class="bline" width="15%" align="right">
						配置值：
					</td>
					<td class="bline" width="85%">
						<input style="width:200px" type="text"  name="configValue" id="configValue" value="${(sysConfig.configValue)!""}">
					</td>
				 </tr>
		 	  	 <tr>
					<td	class="bline" width="15%" align="right">
						备注：
					</td>
					<td class="bline" width="85%">
						<textarea  rows=3 cols=50  name="remark" id="remark">${(sysConfig.remark)!""}</textarea>
					</td>
				 </tr>
		  <tr>
            <td width="1%" height="50"></td> 
            <td width="99%" valign="bottom"> 
            <img src="${contextPath}/resource/image/button_ok.gif" onclick="formSubmit();" style="cursor:pointer" />
				&nbsp;&nbsp;&nbsp;
            <img src="${contextPath}/resource/image/button_back.gif" onclick="javascript:history.back(-1);" width="60" height="22" border="0" />
			</td>
          </tr> 
        </table>
      </td> 