<html> 
<head> 
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'> 
<title>修改</title> 
<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'> 
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
</head> 
<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#111111" style="BORDER-COLLAPSE: collapse"> 
  <tr> 
    <td width="100%" height="20" valign="top"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
        <tr> 
          <td height="30"><IMG height=14 src="${contextPath}/resource/image/book1.gif" width=20> &nbsp;<a href="index.htm"><u>系统配置管理</u></a>&gt;&gt;修改</td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr> 
    <td width="100%" height="1" background="${contextPath}/resource/image/sp_bg.gif"></td> 
  </tr> 

</table> 
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr><td height="10"></td></tr> 
  <tr> 
  	<form name="form1" id='form1' action="${contextPath}/back/system/config/update.htm?id=${sysConfig.id}" method="post">
  		<input type="hidden" name="preConfigKey" value="${sysConfig.configKey}">
		<#include "form.ftl">
  	</form>
  </tr> 

</table> 
</body> 
</html>