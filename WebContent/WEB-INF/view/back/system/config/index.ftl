<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>配置管理</title>
<link href="${contextPath}/resource/css/base.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
	function jumpPage(page) {
		$("#form3").attr("action","${contextPath}/back/system/config/index.htm?currentPage="+page); 
		$("#form3").submit();	
	}
	
	function selAll() {
		var arcIDs = document.getElementsByName("arcID");
		for( i = 0; i < arcIDs.length; i++ ) {
			if( !arcIDs[i].checked ) {
				arcIDs[i].checked = true;
			}
		}
	}
	
	function noSelAll() {
		var arcIDs = document.getElementsByName( "arcID" );
		for ( i = 0; i < arcIDs.length; i++ ) {
			if ( arcIDs[i].checked ) {
				arcIDs[i].checked=false;
			}
		}
	}

	function getCheckboxItem() {
		var allSel = "";
		var arcIDs = document.getElementsByName("arcID");
		for( i = 0; i < arcIDs.length; i++ ) {
			if( arcIDs[i].checked ) {
				if( allSel == "" )
					allSel = arcIDs[i].value;
				else
					allSel = allSel + "," + arcIDs[i].value;
			}
		}
		return allSel;
	}
	
	function deleteData(id){
		if( confirm( "确定要删除吗？" ) ) {
			location.href = "${contextPath}/back/system/config/destory.htm?ids=" + id;
		}
	}
	
	function deleteAll() {
		var ids = getCheckboxItem();
		if( ids == "" ){
			alert( "请选择要删除的数据！" );
		} else {
			if( confirm( "确定要删除数据吗？" ) ) {
				location.href = "${contextPath}/back/system/config/destory.htm?ids=" + ids;
			}
		}
	}
	
	function formSubmit(){
		document.form3.submit();
	}
</script>
<style>
ul {
	padding: 0px;
	margin: 0px;
}

li {
	float: left;
	padding-right: 8px;
	line-height: 24px;
}
</style>
</head>
<body background='${contextPath}/resource/image/allbg.gif' leftmargin='8' topmargin='8'>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#D1DDAA" style="margin-bottom: 6px">
    <tr>
      <td background="${back.imagePath}/wbg.gif" colspan="6">
        <form action='${contextPath}/back/system/config/index.htm' name='form3' id='form3' method='post'>
          <ul>
            <li>配置名称：<input type='text' name='showName' value="${(search.showName)!''}" style='width: 160px' /></li>
            <li>配置关键字：<input type='text' name='configKey' value="${(search.configKey)!''}" style='width: 160px' /></li> &nbsp; &nbsp;
            <li>显示记录数:</li>
            <li><select id="pageSize" name="pageSize">
                <option value='30'<#if search.pageSize=30>selected</#if>>30</option>
                <option value='50'<#if search.pageSize=50>selected</#if>>50</option>
                <option value='100'<#if search.pageSize=100>selected</#if>>100</option>
                <option value='200'<#if search.pageSize=200>selected</#if>>200</option>
            </select></li>
            <li><input id='sousuo' type="submit" value='搜  索' width="60" height="22" border="0" class="np" /></li>
            <ul>
        </form>
      </td>
  </table>
  <table width="98%" border="0" cellpadding="3" cellspacing="1" bgcolor="#D1DDAA" align="center">
    <form name="form2" id='form2' method="post">
      <input type="hidden" name="dopost" value="save">
      <tr>
        <td height="28" colspan="6" background='${contextPath}/resource/image/tbg.gif'>
          <table width="96%" border="0" cellspacing="1" cellpadding="1">
            <tr>
              <td width="24%"><b>系统配置管理</b></td>
              <td width="76%" align="right"><b> <a href="${contextPath}/back/system/config/add.htm"><u>增加配置</u></a>&nbsp;&nbsp; </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr bgcolor="#FDFEE9" align="center">
        <td width="8%" height="24">选择</td>
        <td height="24">配置名称</td>
        <td height="24">配置关键字</td>
        <td height="24">配置值</td>
        <td height="24">备注</td>
        <td width="20%">操作</td>
      </tr>
      <#if page?exists && page.result?size gt 0 > <#list page.result as pr>
      <tr align="center" bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='#f4fbf4';" onMouseOut="javascript:this.bgColor='#FFFFFF';">
        <td height="24"><input type="checkBox" value="${pr.id?if_exists}" name="arcID" id="arcID"></td>
        <td height="24">${pr.showName?if_exists}</td>
        <td height="24">${pr.configKey?if_exists}</td>
        <td height="24">${pr.configValue?if_exists}</td>
        <td height="24">
        	<#if pr.remark?? && pr.remark?length &gt; 15> 
        		${pr.remark[0..10]?if_exists}... 
        	<#else> 
        		${pr.remark?if_exists} 
        	</#if>
        </td>
        <td><a href="${contextPath}/back/system/config/edit.htm?id=${(pr.id)!}">修改</a> | <a href="javascript:deleteData(${(pr.id)!})">删除</a></td>
      </tr>
      </#list> </#if>
    </form>
    <tr bgcolor="#FAFAF1">
      <td height="36" colspan="6">&nbsp; <a href="javascript:selAll()" class="coolbg">全选</a> <a href="javascript:noSelAll()" class="coolbg">取消</a> <a href="javascript:deleteAll()" class="coolbg">&nbsp;删除&nbsp;</a>
      </td>
    </tr>
    <tr>
      <td height='20' align='center' bgcolor='#EEF4EA' colspan="6"><@back.pagination page = page jumpMehtod=selectData /></td>
    </tr>
    <tr>
      <td height="36" bgcolor="#FFFFFF" align="center" colspan="6">页面执行时间:${action.executeTime?default(0)}ms</td>
    </tr>
  </table>
</body>
</html>
