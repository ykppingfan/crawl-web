
<#--分页宏-->
<#macro pagination page jumpMehtod="jumpPage" id="">
	<#if page.totalPages gt 0>
		<div class="pagelistbox">
			<span>共<span style="color:red">${page.totalPages}</span>页/<span style="color:red">${page.totalCount}</span>条记录 </span>
		
			&nbsp;当前第&nbsp;<span style="color:red">${page.pageNo}</span>&nbsp;页&nbsp;
			<a class='nextPage' href='javascript:${jumpMehtod}(1)'>首页 </a>
			<#if page.hasPre>
				<a class='nextPage' href='javascript:${jumpMehtod}(${page.prePage})'>上一页 </a>	
			<#else>
				<span>上一页</span>	
			</#if>
			
			<#if page.hasNext>
				<a class='nextPage' href='javascript:${jumpMehtod}(${page.nextPage})'>下一页 </a>	
			<#else>
				<span>下一页</span>	
			</#if>
			
			<a class='nextPage' href='javascript:${jumpMehtod}(${page.totalPages})'>末页 </a>
			跳转<input type="text" id="_input_page_${(id)!''}" style="width:20px;height:18px;">页
			<script type="text/javascript">
				$("#_input_page_${(id)!''}").keydown(function (evt){
					if(evt.keyCode == 13) {
						var val = $("#_input_page_${(id)!''}").val();
						if(isNaN(val)) {
							$("#_input_page_${(id)!''}").val();
							return;
							
						} else {
							if(parseInt(val) <= 1) {
								val = 1;
							} else if(parseInt(val) >= ${page.totalPages}) {
								val = ${page.totalPages};
							}
							${jumpMehtod}(val);
						}
					}
				});
				$("form").submit(function(){
				
				var flag= $(this).keydown(
				function(evn)
				{
			if(evn.keyCode==13&&!(this.name==='form3'|| this.name==='searchForm'))
				{
				return false;
				}
				return true;
				});
				return flag;
				});
			</script>
		</div>
	</#if>
</#macro>

<#macro hasOper operCode=''>
	<#if action.hasOper(operCode)>
		<#nested true><#t>
	</#if>
</#macro>

<#function replaceHtmlTag source=''>
	<#local res=source?matches("<[^>]*[>||/>]")>
	<#list res as r>
    source?replace(r,"");
	</#list>
	<#return source>
</#function>

<#--公共的路径变量-->
<#assign jsPath='${contextPath}/resource/js' >
<#assign cssPath='${contextPath}/resource/css' >
<#assign imagePath='${contextPath}/resource/image' >