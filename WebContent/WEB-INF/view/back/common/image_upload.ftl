<script type="text/javascript">
//上传图片
function upload(n,waterMarkFlag) {
	var of = $('#uploadFile'+n);
	//检查是否选择了图片
	if(of.val()=='') {
		alert('请选择要上传的图片');
		return;
	}
	//将file移动至上传表单
	$('#fileContent').empty();
	$('#fileContent').append(of);
	//复制一个file放至原处
	$('#spa'+n).append(of.clone());
	//修改属性
	of.attr('id','');
	of.attr('name','uploadFile');
	//其他表单
	if($('#zoom'+n).attr('checked')) {
		$('#ufZoom').val('true');
	} else {
		$('#ufZoom').val('false');
	}
	$('#ufWidth').val($('#zoomWidth'+n).val());
	$('#ufHeight').val($('#zoomHeight'+n).val());
	$('#uploadNum').val(n);
	$("#waterMarkFlag").val(waterMarkFlag);
	var f=$("#uploadForm");
	$("#uploadForm").submit();
}
//剪裁图片
function imgCut(n) {
	if($('#uploadImgPath'+n).val()=='') {
		alert("请先上传图片，再剪裁");
		return;
	}
	if(numberCheck($("#zoomHeight"+n).val())){
		alert("输入的高必须为整数");
		return;
	}
	if(numberCheck($("#zoomWidth"+n).val())){
		alert("输入的宽必须为整数");
		return;
	}
	var url = "${contextPath}/back/image/imgAreaSelect.do?uploadNum="+n+"&imgSrcPath="
		+$("#uploadImgPath"+n).val()+"&zoomWidth="+$("#zoomWidth"+n).val()+"&zoomHeight="+$("#zoomHeight"+n).val() + "&uploadRuleId=${uploadRuleId!''}";
	window.open(url,"imgcut","height=550, width=900, top=auto, left=auto, toolbar=no, menubar=no, scrollbars=auto, resizable=yes,location=no, status=no");
}
//清除图片
function clearImg(n) {
	$('#uploadImgPath'+n).val("");
	$('#preImg'+n).attr("src","${back.imagePath!''}/pview.gif");
}
//reload缩略图
function reloadPreImg(n) {
	$('#preImg' + n).attr('src', $('#preImg' + n).attr('src') + '?' + new Date().getTime()); 
} 
</script>
<form id="uploadForm" action="${contextPath}/back/common/upload/upload.htm" method="post" enctype="multipart/form-data" target="hiddenIframe" style="display:none;width:0px;height:0px;">
	<span id="fileContent"></span>
	<input id="ufZoom" type="hidden" name="zoom"/>
	<input id="ufWidth" type="hidden" name="zoomWidth"/>
	<input id="ufHeight" type="hidden" name="zoomHeight"/>
	<input id="uploadNum" type="hidden" name="uploadNum"/>
	<input type="hidden" name="uploadRuleId" value="${uploadRuleId!''}"/>
	<input type="hidden" name="waterMarkFlag" id="waterMarkFlag" value="${waterMarkFlag?default(1)}"/>
</form>
<iframe name="hiddenIframe" frameborder="0" border="0" style="display:none;width:0px;height:0px;"></iframe>