<html>
	<head>
		<title>提示信息</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<base target='_self'/>
		<style type="text/css">
			div{line-height:160%;}
		</style>
		<script type="text/javascript">
			function JumpUrl(){
				<#if (result.success)! && (result.redirectUrl)??>
					document.location.href = '${(result.redirectUrl)!''}';
				</#if>
  			}
  				
  			window.onload = function () {
  				setTimeout('JumpUrl()', 2000);
  			}
		</script>
	</head>
	<body leftmargin='0' topmargin='0'>
		<br />
		<br />
		<br />
		<br />
		<center>
			<div style='width:450px;padding:0px;border:1px solid #D1DDAA;'>
				<div style='padding:6px;font-size:12px;border-bottom:1px solid #D1DDAA;background:#DBEEBD url(${back.imagePath}/wbg.gif)';'><b>MessageCenter提示信息！</b></div>
				<div style='height:130px;font-size:10pt;background:#ffffff'>
					<br />
					${(result.message)!''}
					<br />
					<#if (result.success)!>
						<a href='${(result.redirectUrl)!"javascript:history.go(-1);"}'>如果你的浏览器没反应，请点击这里...</a>
					<#else>
						<a href='javascript:history.go(-1);'>返回</a>
					</#if>
					
					<br/>
				</div>
				页面执行时间:${(action.executeTime)!0}ms
			</div>
		</center>
	
<#if (result.exception)??>
   <div>
   	${result.exception}
   </div>
</#if>
   
	</body>
</html>

