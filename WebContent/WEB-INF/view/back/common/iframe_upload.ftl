<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
<#if error??>
alert('${error}');
<#else>
var initializationTime = (new Date()).getTime();
parent.document.getElementById('uploadImgPath${uploadNum}').value='${shortImage}';
var imgSrc = parent.document.getElementById('preImg${uploadNum}');
$(imgSrc).css("width","150"); //auto
$(imgSrc).css("height","100"); //auto
$(imgSrc).attr("src",'${imageUrl}');
</#if>
</script>
</head>
<body>
</body>
</html>