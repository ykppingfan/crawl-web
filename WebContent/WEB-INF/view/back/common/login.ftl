<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link type="text/css" href="/resource/css/frame.css" rel="stylesheet" />
		<link type="image/x-icon" href=/resource/image/favicon.ico" rel="shortcut icon" />
		<script type="text/javascript" src="/resource/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="/resource/js//jquery.form.js"></script>
		<title>管理系统 Login...</title>
		<script type="text/javascript">
			function formSubmit() {
				if($('#userName').val() == '') {
					$('#userName').focus();
					$('#msg').addClass('login_error_box');
					$('#msg').text('用户名不能为空');
					return;
				}
				if($('#password').val() == '') {
					$('#password').focus();
					$('#msg').addClass('login_error_box');
					$('#msg').text('密码不能为空');
					return;
				}
				var tempSnPwd = $("#snPwd").is(":hidden");//是否隐藏
				if(tempSnPwd == false) {
					if($('#snPassword').val() == '') {
						$('#snPassword').focus();
						$('#msg').addClass('login_error_box');
						$('#msg').text('动态密码不能为空');
						return;
					}
				}
				
				var temp= $("#authcd").is(":hidden");//是否隐藏 
				if(temp == false){
					if($('#authCode').val() == '') {
						$('#authCode').focus();
						$('#msg').addClass('login_error_box');
						$('#msg').text('验证码不能为空');
						return;
					}
				}
				$('#msg').removeClass('login_error_box');
				$('#msg').addClass('login_info_box');
				$('#msg').text('正在登录...');
				
				$('form').ajaxSubmit({
					url: '${contextPath}/back/common/doLogin.htm',
					type: 'post',
					dataType: 'json',
					success: function(data) {
					    $('#msg').text(data.msg);
						if(data.flag) {
							document.location.href = '${contextPath}/back/frame/main.htm';
						} else {
							if(!data.data){
								$('#authCode').val('');
							    $('#authcd').attr('style',"display:block");
							}
							$('#authCodeImg').attr('src', '${contextPath}/back/common/authCode.htm?r=' + new Date().getTime());
						}
					}
				});
			}
			
			$(document).ready(function() {
				if(top.location != self.location) {
					top.location = self.location;
				}
				$('#userName').focus();
				$('#ok').click(formSubmit);
				$('#cancel').click(function() {
					$('form').reset();
				});
				
				$('#authCodeImg').click(function() {
					$('#authCodeImg').attr('src', '${contextPath}/back/common/authCode.htm?r=' + new Date().getTime());
				});
				
				$('#userName').keydown(function (evt){
					if(evt.keyCode == 13) {
						$('#password').focus();
					}
				});
				
				$('#password').keydown(function (evt){
					var temp= $("#authcd").is(":hidden");
					if(evt.keyCode == 13) {
						if(temp == false){
							$('#authCode').focus();
						}
						if(temp == true){
							formSubmit();
						}
					}
				});
				
				$('#snPassword').keydown(function (evt){
					if(evt.keyCode == 13) {
						formSubmit();
					}
				});
				
				$('#authCode').keydown(function (evt){
					if(evt.keyCode == 13) {
						formSubmit();
					}
				});
			});
			
		</script>
		<style type="text/css">
			<!--
			.vermenu{
				background:#FFFFFF none repeat scroll 0 0;
				border:1px solid #EEEEEE;
				position:absolute;
			}
			-->
		</style>
	</head>
	<body bgcolor="#f4fbf4">
		<div class="head">
  			<div class="top">
    			<div class="top_logo">
    				<img src="${contextPath}/resource/image/admin_top_logo.gif" width="170" height="37" alt="MessageCenter Console Logo" title="Welcome use MessageCenter Console" />
    			</div>
    			<div class="top_link">
      				<ul>
				        <li class="welcome">
				          <script type="text/javascript">
				           	var now=(new Date()).getHours();
							if(now>0&&now<=6){
								document.write("午夜好");
							}else if(now>6&&now<=11){
								document.write("早上好");
							}else if(now>11&&now<=14){
								document.write("中午好");
							}else if(now>14&&now<=18){
								document.write("下午好");
							}else{
								document.write("晚上好");
							}
							</script>
				        </li>
        				<li></li>
      				</ul>
    			</div>
  			</div>
  			<div class="topnav">
    			<div class="menuact" style="width:220px; float:right"></div>
  				</div>
			</div>
			
			<br />
			<div id="login">
  				<div class="theme fLeft">
    				<form name="form1" method="post">
      					<ul>
        					<li>
        						<span>用户名：</span>
          						<input type="text" name="realName" id="userName" class="text" value="" />
        					</li>
        					<li>
        						<span>密&nbsp;&nbsp;&nbsp;码：</span>
          						<input type="password" name="userPwd" id="password" value=""/>
        					</li>
        				<#if openSnCheck?? && openSnCheck.configValue?? && openSnCheck.configValue == "1">
        					<li id='snPwd' style='display:block'>
        				<#else>
        					<li id='snPwd' style='display:none'>
        				</#if>
        						<span>动态密码：</span>
          						<input type="password" name="snPassword" id="snPassword" value="" />
        					</li>
                			<li id='authcd' style='display:none'> 
                				<span>验证码：</span>
           						<input name="authCode" id="authCode" class="text login_from3" type="text" style="width: 50px; text-transform: uppercase;"/>
          						<img src="${contextPath}/back/common/authCode.htm" id="authCodeImg" title="点击刷新" alt="验证码">
        					</li>
					        <li style="padding-top:10px;">
					        	<span>&nbsp;</span>
          						<input id="ok" type="button" value="登录" style='background:url(/resource/image/bt.gif);width:79px;height:24px;border:0px;cursor:pointer;font-size:12px' />
        					</li>
        					<li >
        						<span style="color:red;" id="msg"></span>
        					</li>
      					</ul>
    				</form>
  				</div>
			</div>
		</body>
</html>
