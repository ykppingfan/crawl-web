<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<title>模板配置管理</title>
<link href='${back.cssPath}/base.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="${back.jsPath}/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
	function formSubmit(){
		if($.trim($('#configKey').val())==""){
		   alert("配置关键字不能为空！");
		   return;
		}
		if($.trim($('#configValue').val())==""){
		   alert("配置值不能为空！");
		   return;
		}
		checkForm();
		$("#form1").submit();
	}
		
	function back(){
	   location.href = "${contextPath}/back/gen/index.htm";
	}
    $(document).ready(function(){
        $("#platform").hide();
        $("#product").hide();
        $("#ranking").hide();
        $("#platformAdd").hide();
        $("#productAdd").hide();
        $("#rankingAdd").hide();
        var type = ${config.type?default(1)};
        if(type == 1){
            $("#platform").show();
            $("#platformAdd").show();
        }else if(type == 2){
            $("#product").show();
            $("#productAdd").show();
        }else if(type == 3){
            $("#ranking").show();
            $("#rankingAdd").show();
        }
    });

    function showstatic(){
        $("#platform").hide();
        $("#product").hide();
        $("#ranking").hide();
        $("#platformAdd").hide();
        $("#productAdd").hide();
        $("#rankingAdd").hide();
        var type = $("#type").val();
        if(type == 1){
            $("#platform").show();
            $("#platformAdd").show();
        }else if(type == 2){
            $("#product").show();
            $("#productAdd").show();
        }else if(type == 3){
            $("#ranking").show();
            $("#rankingAdd").show();
        }
    }
    
    function addPlatformRow() {
    	var s = '<tr><td >页数</td><td ><input type="text" name="pageSizePlatform" value=""></td><td >推荐标志</td><td ><select name="commendFlagPlatform"><option value="0" >否</option><option value="1" >是</option></select></td><td>广告位标志</td><td><select name="adFlag"><option value="0" >否</option><option value="1" >是</option></select></td><td ><input onclick="removeRow(this)" type="button" value="删除" class="del coolbt"></td></tr>';
		$('#platformAdd').append( s );	
	}
	function addProductRow() {
    	var s = '<tr><td >页数</td><td ><input type="text" name="pageSizeProduct" value=""></td><td >推荐标志</td><td ><select name="commendFlagProduct"><option value="0" >否</option><option value="1" >是</option></select></td><td ><input onclick="removeRow(this)" type="button" value="删除" class="del coolbt"></td></tr>';
		$('#productAdd').append( s );	
	}
	function addRankingRow() {
    	var s = '<tr><td >页数</td><td ><input type="text" name="pageSizeRanking" value=""></td><td >顺序</td><td ><select name="orderBy"><option value="r.composite desc" >综合排名</option><option value="r.popularity desc" >人气排名</option><option value="r.traffic desc" >流量排名</option><option value="r.praise desc" >口碑排名</option><option value="r.power desc" >实力排名</option><option value="r.trade_num desc" >交易量排名</option><option value="r.interest_rate desc" >利率排名</option><option value="r.invest_passengers desc" >投资人次排名</option><option value="r.loan_passengers desc" >借款人次排名</option><option value="r.invest_money desc" >人均投资金额排名</option><option value="r.loan_money desc" >人均借款金额排名</option></td><td ><input onclick="removeRow(this)" type="button" value="删除" class="del coolbt"></td></tr>';
		$('#rankingAdd').append( s );	
	}
	function removeRow(obj) {
		$(obj).parent().parent().remove();
	}
	
	function checkForm() {
		var type = $("#type").val();
		if(type == 1){
			var json = "[";
			var pageSizePlatform = document.getElementsByName("pageSizePlatform");
			var commendFlagPlatform = document.getElementsByName("commendFlagPlatform");
			var adFlag = document.getElementsByName("adFlag");
			for(i=0;i<pageSizePlatform.length;i++){
				var pageSize = pageSizePlatform[i].value;
				var commendFlag = commendFlagPlatform[i].value;
				var ad = adFlag[i].value;
				if(pageSize=='' || commendFlag=='' || ad==''){
		        	continue;
		        }
				json += "{" + "\"pageSize\"" + ":\"" + pageSize + "\"," + "\"commendFlag\"" + ":\"" + commendFlag + "\"," + "\"adFlag\"" + ":\"" + ad + "\"}";
				if(i<pageSizePlatform.length-1){
					json += ",";
				}
			}
			json += "]";
			$("#configParam").val(json);
		}else if(type == 2){
			var json = "[";
			var pageSizeProduct = document.getElementsByName("pageSizeProduct");
			var commendFlagProduct = document.getElementsByName("commendFlagProduct");
			for(i=0;i<pageSizeProduct.length;i++){
				var pageSize = pageSizeProduct[i].value;
				var commendFlag = commendFlagProduct[i].value;
				if(pageSize=='' || commendFlag==''){
		        	continue;
		        }
				json += "{" + "\"pageSize\"" + ":\"" + pageSize + "\"," + "\"commendFlag\"" + ":\"" + commendFlag + "\"}";
				if(i<pageSizeProduct.length-1){
					json += ",";
				}
			}
			json += "]";
			$("#configParam").val(json);
		}else if(type == 3){
			var json = "[";
			var pageSizeRanking = document.getElementsByName("pageSizeRanking");
			var orderBy = document.getElementsByName("orderBy");
			for(i=0;i<pageSizeRanking.length;i++){
				var pageSize = pageSizeRanking[i].value;
				var order = orderBy[i].value;
				if(pageSize=='' || order==''){
		        	continue;
		        }
				json += "{" + "\"pageSize\"" + ":\"" + pageSize + "\"," + "\"orderBy\"" + ":\"" + order + "\"}";
				if(i<pageSizeRanking.length-1){
					json += ",";
				}
			}
			json += "]";
			$("#configParam").val(json);
		}
	}
</script>

</head>
<body leftmargin='15' topmargin='10' bgcolor="#FFFFFF">
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr><td height="10"></td></tr>
	<#--常规信息-->
	<table width="100%" border="0" cellspacing="0" id="head1" cellpadding="0" style="border-bottom:1px solid #CCCCCC">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF">
	    <table height="24" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	          <td width="84" height="24" align="center" background="${contextPath}/resource/image/itemnote1.gif">&nbsp;常规信息&nbsp;</td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
      <form name="form1" id='form1' action="${contextPath}/back/gen/save.htm" method="post">
          <input name="id" type="hidden" value="${(config.id)!''}"/>
	      <td height="95" align="center" bgcolor="#FFFFFF"> 
	        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="bline" width="20%" align="right">
                        配置关键字：
                    </td>
                    <td class="bline" width="80%" colspan="4">
                        <input style="width:200px" type="text" name="configKey" id="configKey"
                               value="${(config.configKey)!''}" maxlength="32">
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="20%" align="right">
                        模版类型：
                    </td>
                    <td class="bline" width="80%" colspan="4">
                        <select id="type" name="type" style="width:200px" onchange="showstatic()">
                            <option value="1" <#if config.type?? && config.type=1>selected</#if>>平台</option>
                            <option value="2" <#if config.type?? && config.type=2>selected</#if>>产品</option>
                            <option value="3" <#if config.type?? && config.type=3>selected</#if>>排行</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="20%" align="right">
                        生成模版文件名称：
                    </td>
                    <td class="bline" width="80%" colspan="4">
                        <input type="text" style="width:200px" name="ftlName" id="ftlName" value="${config.ftlName!''}">
                        &nbsp;<font color="#a9a9a9"></font><font color="red">xxx.ftl</font>
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="20%" align="right">
                        生成静态文件名称：
                    </td>
                    <td class="bline" width="80%" colspan="4">
                        <input type="text" style="width:200px" name="htmlName" id="htmlName" value="${config.htmlName!''}">
                        &nbsp;<font color="#a9a9a9">如：/cngold.org/p2p/appfile/</font><font color="red">xxx.html</font>
                    </td>
                </tr>
                <tr>
                    <td class="bline" width="20%" align="right">
                        备注：
                    </td>
                    <td class="bline" width="80%" colspan="4">
                        <textarea rows=3 cols=50 name="describe" id="describe">${(config.describe)!""}</textarea>
                    </td>
                </tr>
                <tr >
                    <td height="24" class="bline" colspan="4">
                		<#--<#if config.type?exists && config.type == 1>-->
                		<#if config?exists && config.configList?exists&&config.configList?size &gt; 0>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="platformAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addPlatformRow();" id="addPlatformAttr">
			                    </td>
			                </tr>
			                <#list config.configList as temp> 
			                <tr>
		                        <td class="bline" width="20%">页数
		                        </td>
		                        <td >
		                            <input type="text" name="pageSizePlatform" value="${(temp.pageSize)!''}">
		                        </td>
		                         <td class="bline" width="20%">推荐标志
		                        </td>
		                        <td >
		                        	<select name="commendFlagPlatform">
		                        		<option value="0" <#if (temp.commendFlag)?? && temp.commendFlag == '0'>selected="selected"</#if>>否</option>
		                        		<option value="1" <#if (temp.commendFlag)?? && temp.commendFlag == '1'>selected="selected"</#if>>是</option>
		                        	</select>
		                        </td>
		                        <td>广告位标志</td>
		                        <td>
		                        	<select name="adFlag">
		                        		<option value="0" <#if (temp.adFlag)?? && temp.adFlag == '0'>selected="selected"</#if>>否</option>
		                        		<option value="1" <#if (temp.adFlag)?? && temp.adFlag == '1'>selected="selected"</#if>>是</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
		                     </#list>
                    	</table>
                    	<#else>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="platformAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addPlatformRow();" id="addPlatformAttr">
			                    </td>
			                </tr>
			                <tr>
		                        <td class="bline" width="20%">页数
		                        </td>
		                        <td >
		                            <input type="text" id="pageSizePlatform" name="pageSizePlatform" value="">
		                        </td>
		                         <td class="bline" width="20%">推荐标志
		                        </td>
		                        <td >
		                        	<select id="commendFlagPlatform" name="commendFlagPlatform">
		                        		<option value="0" >否</option>
		                        		<option value="1" >是</option>
		                        	</select>
		                        </td>
		                        <td>广告位标志</td>
		                        <td>
		                        	<select id="adFlag" name="adFlag">
		                        		<option value="0" >否</option>
		                        		<option value="1" >是</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
                    	</table>
                    	</#if>
                    	<#--<#elseif config.type?exists && config.type == 2>-->
                    	<#if config?exists && config.configList?exists&&config.configList?size &gt; 0>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="productAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addProductRow();" id="addProductAttr">
			                    </td>
			                </tr>
			                <#list config.configList as temp> 
			                <tr>
		                        <td class="bline" width="20%">页数
		                        </td>
		                        <td >
		                                  <input type="text" name="pageSizeProduct" value="${(temp.pageSize)!''}">
		                        </td>
		                         <td class="bline" width="20%">推荐标志
		                        </td>
		                        <td >
		                        	<select name="commendFlagProduct">
		                        		<option value="0" <#if (temp.commendFlag)?? && temp.commendFlag == '0'>selected="selected"</#if>>否</option>
		                        		<option value="1" <#if (temp.commendFlag)?? && temp.commendFlag == '1'>selected="selected"</#if>>是</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
		                     </#list>
                    	</table>
                    	<#else>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="productAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addProductRow();" id="addProductAttr">
			                    </td>
			                </tr>
			                <tr>
		                        <td >页数
		                        </td>
		                        <td >
		                            <input type="text" id="pageSizeProduct" name="pageSizeProduct" value="">
		                        </td>
		                         <td >推荐标志
		                        </td>
		                        <td >
		                        	<select id="commendFlagProduct" name="commendFlagProduct">
		                        		<option value="0" >否</option>
		                        		<option value="1" >是</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
                    	</table>
                    	</#if>
                    	<#--<#elseif config.type?exists && config.type == 3>-->
                    	<#if config?exists && config.configList?exists&&config.configList?size &gt; 0>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="rankingAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addRankingRow();" id="addRankingAttr">
			                    </td>
			                </tr>
			                <#list config.configList as temp> 
			                <tr>
		                        <td width="12%">页数
		                        </td>
		                        <td >
		                             <input type="text" name="pageSizeRanking" value="${(temp.pageSize)!''}">
		                        </td>
		                         <td >顺序
		                        </td>
		                        <td >
		                        	<select name="orderBy">
		                        		<option value="r.composite desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.composite desc'>selected="selected"</#if>>综合排名</option>
		                        		<option value="r.popularity desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.popularity desc'>selected="selected"</#if>>人气排名</option>
		                        		<option value="r.traffic desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.traffic desc'>selected="selected"</#if>>流量排名</option>
		                        		<option value="r.praise desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.praise desc'>selected="selected"</#if>>口碑排名</option>
		                        		<option value="r.power desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.power desc'>selected="selected"</#if>>实力排名</option>
		                        		<option value="r.trade_num desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.trade_num desc'>selected="selected"</#if>>交易量排名</option>
		                        		<option value="r.interest_rate desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.interest_rate desc'>selected="selected"</#if>>利率排名</option>
		                        		<option value="r.invest_passengers desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.invest_passengers desc'>selected="selected"</#if>>投资人次排名</option>
		                        		<option value="r.loan_passengers desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.loan_passengers desc'>selected="selected"</#if>>借款人次排名</option>
		                        		<option value="r.invest_money desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.invest_money desc'>selected="selected"</#if>>人均投资金额排名</option>
		                        		<option value="r.loan_money desc" <#if (temp.orderBy)?? && temp.orderBy == 'r.loan_money desc'>selected="selected"</#if>>人均借款金额排名</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
		                     </#list>
                    	</table>
                    	<#else>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="rankingAdd">
                    		<tr>
			                    <td>
			                    <input type="button" value="新增筛选条件" onclick="addRankingRow();" id="addRankingAttr">
			                    </td>
			                </tr>
			                <tr>
		                        <td width="12%">页数
		                        </td>
		                        <td >
		                             <input type="text" id="pageSizeRanking" name="pageSizeRanking" value="">
		                        </td>
		                         <td >顺序
		                        </td>
		                        <td >
		                        	<select id="orderBy" name="orderBy">
		                        		<option value="r.composite desc" >综合排名</option>
		                        		<option value="r.popularity desc" >人气排名</option>
		                        		<option value="r.traffic desc" >流量排名</option>
		                        		<option value="r.praise desc" >口碑排名</option>
		                        		<option value="r.power desc" >实力排名</option>
		                        		<option value="r.trade_num desc" >交易量排名</option>
		                        		<option value="r.interest_rate desc" >利率排名</option>
		                        		<option value="r.invest_passengers desc" >投资人次排名</option>
		                        		<option value="r.loan_passengers desc" >借款人次排名</option>
		                        		<option value="r.invest_money desc" >人均投资金额排名</option>
		                        		<option value="r.loan_money desc" >人均借款金额排名</option>
		                        	</select>
		                        </td>
		                        <td >
		                        <input onclick="removeRow(this)" type="button" value="删除" class="del coolbt">
		                        </td>
		                     </tr>
                    	</table>
                    	</#if>
                    	<#--<#else>-->
                    	
                    	
                    	
                    	<#--</#if>-->
                  </td>
                  <#--
                  <td align="left">
                        <font id="platform" color="#4169e1">如： [{"pageSize":5,"commendFlag":1,"adFlag":1}]</font>
                        <font id="product" style="display: none" color="#4169e1">如： [{"pageSize":5,"commendFlag":1}]</font>
                        <font id="ranking" style="display: none" color="#4169e1">如： [{"pageSize":5,"orderBy":" r.composite desc "},{"pageSize":5,"orderBy":" r.interest_rate desc "}]
                        <br><font color="#808080">维度有： composite(综合排名) popularity(人气排名) traffic(流量排名) praise(口碑排名) power(实力排名)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            trade_num(交易量排名) interest_rate(利率排名) invest_passengers(投资人次排名) loan_passengers(借款人次排名)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            invest_money(人均投资金额排名) loan_money(人均借款金额排名)</font>
                            </font>
                    </td>
                  -->
                </tr>
                <input type="hidden" name="configParam" id="configParam" value="${(config.configParam)!''}" />
				<tr>
					<td  width="20%" align="right" >
						模版内容：
					</td>
					<td colspan="4"  width="80%">
                        <textarea rows=30 cols=150 name="configValue" id="configValue">${(config.configValue)!""}</textarea>
					</td>
				</tr>
                <tr>
                    <td width="1%" height="50"></td>
                    <td width="99%" valign="bottom" colspan="4">
                        <input type="button" value="保存" onclick="formSubmit();" style="cursor:pointer;">&#160;&#160;&#160;
                        <input type="button" value="返回列表" onClick="back();" style="cursor:pointer;">
                    </td>
                </tr>
			</table>
	      </td> 
      </form>
    </tr>
  </table>
</body>
</html>