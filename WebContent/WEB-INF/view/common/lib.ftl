<#--前台调用带样式的分页宏-->
<#macro paginationFront jumpMehtod secondUrl suffixUrl page=page>
	<#if page.totalPages gt 0>
    <div class="digg clearfix">
		<span class="disabled">
			<a href='${jumpMehtod}${suffixUrl}'>首页</a>
		<#--计算初始页码和结束页码-->
			<#if page.pageNo-1 lte 2>
				<#assign startNo=1>
			<#else>
				<#assign startNo=page.pageNo-1>
			</#if>

			<#if page.totalPages-startNo-5 gt 0>
				<#assign endNo=page.totalPages-startNo-5 >
			<#else>
				<#assign endNo=page.totalPages>
			</#if>
		<#--10个页码-->
			<#assign pgNo=startNo>
			<#if pgNo==page.pageNo>
				<span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
				<a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
				<span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
				<a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
				<span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
				<a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
				<span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
				<a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
				<span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
				<a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<#assign pgNo=pgNo+1 >
			<#if pgNo==page.pageNo>
                <span class="thisclass">${pgNo}</span>
			<#elseif pgNo lte page.totalPages >
                <a href="<#if pgNo gt 1>${secondUrl}_p${pgNo}.htm<#else>${jumpMehtod}${suffixUrl}</#if>">${pgNo}</a>
			</#if>

			<a href='<#if page.totalPages gt 1>${secondUrl}_p${page.totalPages}.htm<#else>${jumpMehtod}${suffixUrl}</#if>'>末页 </a>
			<span class="pageinfo">共<strong>${page.totalPages}</strong>页 </span>
		</span>
    </div>
	</#if>
</#macro>
<#--分页宏-->
<#macro pagination page=inner.pageStatus.page style='common' jumpMehtod="jumpPage">
	<#if page.totalPages gt 0>
		<div class="digg">
			<span class="disabled">
				<#if page.hasPre>
					<a  href='javascript:${jumpMehtod}(1)'>首页 </a>
					<a href='javascript:${jumpMehtod}(${page.prePage})'>上一页 </a>
				<#else>
					<a href='javascript:${jumpMehtod}(1)'>首页</a>
				</#if>
				
				<#if page.totalPages &gt; 10 && page.pageNo &gt; 9>
					<#list 1..page.totalPages as c>
						<#if c_index &gt; (page.pageNo - 6) && c_index &lt; (page.pageNo + 4)>
							<#if c == page.pageNo>
								<span class="thisclass">${c}</span>
							<#else>
								<a href='javascript:${jumpMehtod}(${c})'>${c}</a>
							</#if>
						</#if>
					</#list>
				<#else>
					<#list 1..page.totalPages as c>
						<#if c == page.pageNo>
							<span class="thisclass">${c}</span>
						<#else>
							<#if c_index &gt; (page.pageNo - 6) && c_index &lt; (page.pageNo + 4)>
								<a href='javascript:${jumpMehtod}(${c})'>${c}</a>
							</#if>
						</#if>
					</#list>
				</#if>
				
				<#if page.hasNext>
					<a  href='javascript:${jumpMehtod}(${page.nextPage})'>下一页 </a>
					<a href='javascript:${jumpMehtod}(${page.totalPages})'>末页 </a>
				<#else>
					<a href='javascript:${jumpMehtod}(${page.totalPages})'>末页</a>
				</#if>
				<span class="pageinfo">共 <strong>${page.totalPages}</strong>页<strong>${page.totalCount}</strong>条</span>
			</span>
		</div>
	</#if>
</#macro>
<#--直接调用模板string
<@back.getTemplats key="test2">
</@back.getTemplats>
-->
<#macro getTemplates key=''>
	<#local temp= spring.templateSerivce.getTemplats(key)>
${temp}
</#macro>

<#--直接调用模板string
<@back.static key="test2">
</@back.static>
-->
<#macro static key=''>
	<#local temp= spring.templateSerivce.statics(key)>
${temp}
</#macro>

<#--直接调用模板string
<@back.dynamic key="test2">
</@back.dynamic>
-->
<#macro dynamic key=''>
	<#local temp= spring.templateSerivce.dynamic(key)>
${temp}
</#macro>