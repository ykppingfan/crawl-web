<#include 'common.ftl'>
<#-- 普通文章文档(doc) -->
<#macro articleDoc
	websiteId=[] <#--站点Id。例如：列出站点ID[2, 3, 4]下的所有文章-->
	docId=[] <#--文档Id。列出指定文档[888, 889, 901]的文档-->
	flag=[] <#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[] <#--自定义属性。不包含集合中的属性。-->
	channelId=[] <#--栏目Id。例如：列出[23, 34, 89]下的所有文档。-->
	recursion=true <#--是否递归列出channelId下的子栏目的所有文档。-->
	beginTime='' <#--指定发布的开始时间 yyyy-mm-dd-->
	endTime='' <#--指定发布的结束时间 yyyy-mm-dd-->
	day=-1 <#--指定day天内发布的文档-->
	tagName=[] <#--指定Tag的文档。例如：指定一个或多个tag的文档。例如：列出[1, 3]的文档-->
	keyword=[] <#--指定包含在集合中的关键字的文档。['黄金', '股票']-->
	condition='' <#--自定义查询条件-->
	orderBy='publish_time desc' <#--排序方式-->
	limit=[0,10] <#--行数 [0, 10]或[10]都是从列出前10条数据-->
	pagination=false <#--是否分页-->
	pageSize=10 <#--如果启用分页 每页记录数 默认 每页10条-->
	currentPage=(inner.pageStatus.currentPage)!1 <#--当前页数-->
	showDelete=false <#--是否也把回收站中的文章也展示出来 默认不展示-->
	random=false
	websiteGroupId=[]
	organizationId=[]
	expertId = []
	maxPageCount=(inner.pageStatus.maxPageCount)!10
	lazyload=[]
	noIds=[]<#--排除ID-->
	regionId=[]
	bankId=[]
>
	<#local page=taglib.tlArticleDocService.ftlFindAll(websiteId, docId, flag, noFlag, channelId,
			recursion, beginTime, endTime, day,
			tagName, keyword, condition, orderBy, limit, 
			pagination, pageSize,
			currentPage,random,websiteGroupId,
			organizationId, expertId, maxPageCount,lazyload,noIds,regionId,bankId)>
	<#if pagination?? && pagination>
		${(inner.pageStatus.setPageSize(pageSize))!}
		${(inner.pageStatus.setPageCount(page.totalPages))!}
		${(inner.pageStatus.setPage(page))!}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>

</#macro>

<#-- 官方网站文档(doc) -->
<#macro officialDoc
	websiteId=[] <#--站点Id。例如：列出站点ID[2, 3, 4]下的所有文章-->
	docId=[] <#--文档Id。列出指定文档[888, 889, 901]的文档-->
	flag=[] <#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[] <#--自定义属性。不包含集合中的属性。-->
	channelId=[] <#--栏目Id。例如：列出[23, 34, 89]下的所有文档。-->
	recursion=true <#--是否递归列出channelId下的子栏目的所有文档。-->
	beginTime='' <#--指定发布的开始时间 yyyy-mm-dd-->
	endTime='' <#--指定发布的结束时间 yyyy-mm-dd-->
	day=-1<#--指定day天内发布的文档-->
	tagName=[] <#--指定Tag的文档。例如：指定一个或多个tag的文档。例如：列出[1, 3]的文档-->
	keyword=[] <#--指定包含在集合中的关键字的文档。['黄金', '股票']-->
	condition='' <#--自定义查询条件-->
	orderBy='publish_time desc' <#--排序方式-->
	limit=[0,10] <#--行数 [0, 10]或[10]都是从列出前10条数据-->
	pagination=false <#--是否分页-->
	pageSize=10 <#--如果启用分页 每页记录数 默认 每页10条-->
	currentPage=(inner.pageStatus.currentPage)!1 <#--当前页-->
	showDelete=false <#--是否也把回收站中的文章也展示出来 默认不展示-->
	random=false
	websiteGroupId=[]
	maxPageCount=(inner.pageStatus.maxPageCount)!10
	lazyload=[]
	noIds=[] <#--排除ID-->
	bankId=[]
>
	<#local page=taglib.tlOfficialDocService.ftlFindAll(websiteId, docId, flag, noFlag, channelId,
			recursion, beginTime, endTime, day,
			tagName, keyword, condition, orderBy, limit, 
			pagination, pageSize,currentPage,random,websiteGroupId,maxPageCount,lazyload,noIds,bankId)>

	<#if pagination?? && pagination>
		${(inner.pageStatus.setPageSize(pageSize))!}
		${(inner.pageStatus.setPageCount(page.totalPages))!}
		${(inner.pageStatus.setPage(page))!}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>
</#macro>

<#-- 专题文档(doc) -->
<#macro specDoc
	websiteId=[] <#--站点Id。例如：列出站点ID[2, 3, 4]下的所有文章-->
	docId=[] <#--文档Id。列出指定文档[888, 889, 901]的文档-->
	flag=[] <#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[] <#--自定义属性。不包含集合中的属性。-->
	channelId=[] <#--栏目Id。例如：列出[23, 34, 89]下的所有文档。-->
	recursion=true <#--是否递归列出channelId下的子栏目的所有文档。-->
	beginTime='' <#--指定发布的开始时间 yyyy-mm-dd-->
	endTime='' <#--指定发布的结束时间 yyyy-mm-dd-->
	day=-1 <#--指定day天内发布的文档-->
	tagName=[] <#--指定Tag的文档。例如：指定一个或多个tag的文档。例如：列出[1, 3]的文档-->
	keyword=[] <#--指定包含在集合中的关键字的文档。['黄金', '股票']-->
	condition='' <#--自定义查询条件-->
	orderBy='publish_time desc' <#--排序方式-->
	limit=[0,10] <#--行数 [0, 10]或[10]都是从列出前10条数据-->
	pagination=false <#--是否分页-->
	pageSize=10 <#--如果启用分页 每页记录数 默认 每页10条-->
	currentPage=(inner.pageStatus.currentPage)!1 <#--当前页数-->
	showDelete=false <#--是否也把回收站中的文章也展示出来 默认不展示-->
	random=false
	websiteGroupId=[]
	maxPageCount=(inner.pageStatus.maxPageCount)!10
	lazyload=[]
	noIds=[] <#--排除ID-->
	bankId=[]
>
	<#local page=taglib.tlSpecDocService.ftlFindAll(websiteId, docId, flag, noFlag, channelId,
			recursion, beginTime, endTime, day,
			tagName, keyword, condition, orderBy, limit, 
			pagination, pageSize,currentPage,random,websiteGroupId, maxPageCount,lazyload,noIds,bankId)>

	<#if pagination?? && pagination>
		${(inner.pageStatus.setPageSize(pageSize))!}
		${(inner.pageStatus.setPageCount(page.totalPages))!}
		${(inner.pageStatus.setPage(page))!}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>
</#macro>

<#-- 优惠活动文档(doc) -->
<#macro activityDoc
	websiteId=[] <#--站点Id。例如：列出站点ID[2, 3, 4]下的所有文章-->
	docId=[] <#--文档Id。列出指定文档[888, 889, 901]的文档-->
	flag=[] <#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[] <#--自定义属性。不包含集合中的属性。-->
	channelId=[] <#--栏目Id。例如：列出[23, 34, 89]下的所有文档。-->
	recursion=true <#--是否递归列出channelId下的子栏目的所有文档。-->
	beginTime='' <#--指定发布的开始时间 yyyy-mm-dd-->
	endTime='' <#--指定发布的结束时间 yyyy-mm-dd-->
	day=-1 <#--指定day天内发布的文档-->
	tagName=[] <#--指定Tag的文档。例如：指定一个或多个tag的文档。例如：列出[1, 3]的文档-->
	keyword=[] <#--指定包含在集合中的关键字的文档。['黄金', '股票']-->
	condition='' <#--自定义查询条件-->
	orderBy='publish_time desc' <#--排序方式-->
	limit=[0,10] <#--行数 [0, 10]或[10]都是从列出前10条数据-->
	pagination=false <#--是否分页-->
	pageSize=10 <#--如果启用分页 每页记录数 默认 每页10条-->
	currentPage=(inner.pageStatus.currentPage)!1 <#--当前页数-->
	showDelete=false <#--是否也把回收站中的文章也展示出来 默认不展示-->
	random=false
	websiteGroupId=[]
	maxPageCount=(inner.pageStatus.maxPageCount)!10
	regionId=[]
	brandId=[]
	lazyload=[]
	noIds=[] <#--排除ID-->
	bankId=[]
>
	<#local page=taglib.tlActivityDocService.ftlFindAll(websiteId, docId, flag, noFlag, channelId,
			recursion, beginTime, endTime, day,
			tagName, keyword, condition, orderBy, limit, 
			pagination, pageSize,
			currentPage,random,websiteGroupId, maxPageCount,regionId,brandId,lazyload,noIds,bankId)>

	<#if pagination?? && pagination>
		${inner.pageStatus.setPageSize(pageSize)}
		${inner.pageStatus.setPageCount(page.totalPages)}
		${inner.pageStatus.setPage(page)}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>
</#macro>