<#include 'common.ftl'>
<#-- 信用卡 -->
<#macro credit
	creditId=[]		<#--信用卡Id。列出指定信用卡[888, 889, 901]的文档-->
	noCreditId=[]
	flag=[] 			<#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[] 			<#--自定义属性。不包含集合中的属性。-->
	tagId=[] 			<#--指定Tag的文档。例如：指定一个或多个tag的文档。例如：列出[1, 3]的文档-->
	keyword=[] 			<#--指定包含在集合中的关键字的文档。['广发', '金卡']-->
	orderBy='id desc' 	<#--排序方式-->
	random=false 
	limit=[] 			<#--行数 [0, 10]或[10]都是从列出前10条数据-->
	screeningId=[]
	bankId=[]
	isApply=2
	regionId=[]
	activityId=[]
>
	<#local page=taglib.ftlCreditCreditcardService.findPage(creditId,noCreditId,flag,noFlag,tagId,keyword,orderBy,random,limit,screeningId,bankId,isApply,regionId,activityId)>
	<#if pagination?? && pagination>
		${(inner.pageStatus.setPageSize(pageSize))!}
		${(inner.pageStatus.setPageCount(page.totalPages))!}
		${(inner.pageStatus.setPage(page))!}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>
</#macro>

<#-- 优惠活动 -->
<#macro activity 	
	activityId=[]		<#--信用卡Id。列出指定信用卡[888, 889, 901]的文档-->
	noActivityId=[]
	flag=[] 			<#--自定义属性。头条:h 推荐:c 幻灯:f 特荐:a 滚动:s 加粗: b 图片: p 跳转: j。默认列出全部。例如：列出 头条和幻灯 ['h', 'c']-->
	noFlag=[]
	tagId=[] 			<#--自定义属性。不包含集合中的属性。-->
	keyword=[] 			<#--指定包含在集合中的关键字的文档。['广发', '金卡']-->
	orderBy='id desc' 	<#--排序方式-->
	random=false 
	limit=[] 			<#--行数 [0, 10]或[10]都是从列出前10条数据-->
	brandId=[]         <#--商户id-->
	bankId=[]           <#--银行id-->
	screeningId=[]     	<#--塞选条件id-->
	regionId=[]			<#--城市id-->
	creditCardId=[]
	isSelf=2
>
	<#local page=taglib.ftlCreditActivityService.findPage(activityId,noActivityId,flag,noFlag,tagId,keyword,orderBy,random,limit,brandId,bankId,screeningId,regionId,creditCardId,isSelf)>
		<#if pagination?? && pagination>
		${(inner.pageStatus.setPageSize(pageSize))!}
		${(inner.pageStatus.setPageCount(page.totalPages))!}
		${(inner.pageStatus.setPage(page))!}
	</#if>
	<#list page.result as e>
		<#nested e, iteratorStatus(e_index, page.result?size)><#t>
	</#list>
</#macro>

