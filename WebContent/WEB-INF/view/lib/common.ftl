<#--
返回一个迭代器状态对象
-->
<#function iteratorStatus index size>
	<#local e={'index': index, 'count': index + 1, 'first': index = 0, 'last': index + 1 = size, 'odd': index % 2 = 1, 'even': index % 2 = 0, 'size': size} />
  	<#return e>
</#function>