<!DOCTYPE html>
<html style="width: 100%;">
<head>
<meta charset="UTF-8">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="telephone=no" name="format-detection">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<title>黄金宝</title>

<link rel="stylesheet" href="${path}/css/public.css">

<script type="text/javascript" src="${path}/js/jquery1.11.1.js"></script>
<style>

/*页头*/
.heade-return {
	position: absolute;
	width: 12px;
	height: 22px;
	background: url(${path}/images/fanhui.png) center center no-repeat;
	background-size: 100%;
	top: 12px;
	left: 10px;
}
/*正文*/
.content-sell {
	border-bottom: #ccc solid 1px;
	border-top: #ccc solid 1px;
	margin-top: 0.4rem;
	background-color: #fff;
	padding-bottom: 6px;
	text-align: center;
	padding-top: 0.5rem;
	padding-bottom: 0.5rem;
}

.content-dep {
	/* background-color: #fff; */
}

.content-dep li {
	margin: 0.4rem auto 0px;
	width: 90%;
	position: relative;
	line-height: 2.2rem;
	font-size: 16px;
	padding-top: 1rem;
}

.content-dep li:last-child {
	border: none;
	line-height: 1rem;
	word-break: break-all;
	word-wrap: break-word;
	font-size: 0.7rem;
	margin-top: 0px;
	padding-top: 0rem;
}

.content-dep label {
	position: absolute;
}

.content-dep span {
	margin-left: 2rem;
	font-size: 0.7rem;
}

.content-dep .down-arrow {
	position: absolute;
	right: 16px;
	display: block;
	width: 0.4rem;
	height: 0.8rem;
	top: 0.1rem;
}

.content-dep .down-arrow img {
	width: 0.4rem;
	line-height: 0px;
}

.content-sellText {
	padding-top: 0.4rem;
	font-size: 14px;
	margin-left: 20px;
}

.deposit-btn {
	margin-left: 2rem;
	margin-right: 2rem;
	margin-top: 0.5rem;
	height: 40px;
	background-color: #fff;
	text-align: center;
	line-height: 40px;
	border-radius: 4px;
	color: #ff4c4c;
	font-size: 18px;
}
.content{padding-top:0px;}
</style>
</head>
<body id="body">
	<div id="div1" class="mask">
		<!--加载动画-->
		<div class="loader-inner line-scale">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>

		</div>
	</div>
	<div id="bodys" style="display: none;">
		<!--页头-->
		<c:if test="${type=='h5' }">
		<section class="heade">
			<div class="heade-return" onclick="javascript:history.go(-1)"></div>
			关于黄金宝
		</section>
		<style>
				.content{
					padding-top:45px;
				}
			</style>
			</c:if>
		<!--正文-->
		<section id="content" class="content" style="padding-bottom: 0px;">

			<ul class="content-dep">
				<li><img src="${path}/images/icongold.png"
					style="width: 3rem; display: block; margin: 0 auto;">
					<p style="text-align: center; color: #007AFF; font-size: 0.7rem;">黄金宝</p>
				</li>
				<li>
				黄金宝是由浙江黄金宝投资股份有限公司倾力打造的创新型互联网黄金O2O交易平台。“黄金宝”包括网站端（www.zjhjb.com）及app端，开展面向普通黄金投资者的买金、卖金、存金、提金服务，价格为上海黄金交易所实时金价。
				<p>黄金宝具有投资门槛低、操作便捷、流动性强、收益率高、安全稳健等平台优势，极大地满足了用户灵活增值黄金的投资需求，拉近了普通消费者和投资者与黄金的距离，并通过闲置黄金市场再流通的创新投资模式，使平台用户的黄金资产日日生息，带来额外收益。
				目前黄金宝线下有300余家黄金实体店，为广大用户提供存金提金业务。</p>
				黄金宝已和中国人保财险、太平洋财险签订协议，为所有用户账户、物流配送、库存黄金进行投保。同时资金由第三方银行进行监管，并委托国信证券、平安信托等提供资产增值服务。
				</li>
			</ul>

		</section>
	</div>
	<script src="${path}/js/scree.js"></script>
	<script language="javascript">
		window.onload = function() {
			screen() /************************屏幕识别*********************/
			loads();
			/************************页面动画加载*********************/

		}
	</script>
</body>
</html>

