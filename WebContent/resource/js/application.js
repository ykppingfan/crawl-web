function blockpageCenter(msg) {
	$.blockUI({
		css : {
			border : 'none',
			padding : '15px',
			backgroundColor : '#000',
			'-webkit-border-radius' : '10px',
			'-moz-border-radius' : '10px',
			opacity : .5,
			color : '#fff'
		},
		overlayCSS : {
			backgroundColor : '#fff'
		},
		message : msg
	});
}

function blockpageCenter4Det(msg) {
	$.blockUI({
		css : {
		    top:"60px",
			padding : '15px',
			'-webkit-border-radius' : '10px',
			'-moz-border-radius' : '10px',
			'text-align' : 'left',
			'font-size' : '12px',
			'cursor' : 'pointer',
			'height':'300px',
			'overflow':'auto'
		},
		overlayCSS : {
			backgroundColor : '#fff'
		},
		message : msg
	});
}

function blockpageTop(msg,width,left) {
	$.blockUI({
		centerY : 0,
		css : {
			top : '100px',
			padding : '15px',
			left:left,
			width : width,
			'-webkit-border-radius' : '10px',
			'-moz-border-radius' : '10px',
			'font-size' : '12px',
			'cursor' : 'pointer'
		},
		overlayCSS : {
			backgroundColor : '#fff'
		},
		message : msg
	});

}

function blockpageRight(msg) {
	$.blockUI({
		centerY : 0,
		css : {
			top : '10px',
			left : '',
			right : '10px',
			backgroundColor : '#fedbd1',
			color : '#e40505',
			border : '1px solid #ff0000',
			width : '200px',
			height : '20px'
		},
		overlayCSS : {
			backgroundColor : '#fff'
		},
		message : msg
	});
}

function unblockpage() {
	$.unblockUI();
}

function blockGrowlUI(message) {
	$.growlUI('温馨提示', message);
}

/**
 * 选择所有选项
 * 
 * @param check
 * @param checkboxName
 * @return
 */
function selectAllCheckbox(check, checkboxName) {
	var checkboxs = document.getElementsByName(checkboxName);
	for (var i = 0; i < checkboxs.length; i++) {
		checkboxs[i].checked = check.checked;
	}
}

/**
 * 获取选择项
 * 
 * @param checkboxName
 * @return
 */
function getSelectCheckbox(checkboxName) {
	var checkboxs = document.getElementsByName(checkboxName);
	var select = "";
	for (var i = 0; i < checkboxs.length; i++) {
		if (checkboxs[i].checked) {
			select += checkboxs[i].value + ",";
		}
	}
	return select;
}



function selectDate_yymmddHHMMSS(id) {
    selectDateCal(id, "%Y-%m-%d %H:%M:%S", true)
}

function selectDate_yymmddHHMM(id) {
   selectDateCal(id, "%Y-%m-%d %H:%M", true)
}

function selectDate_yymmdd(id) {
	   selectDateCal(id, "%Y-%m-%d", false)
}

function selectDate_yymm(id) {
	   selectDateCal(id, "%Y-%m", false)
}


function selectDateCal(id, format, isTime) {
	 var cal = Calendar.setup({
      onSelect: function() {            			 
                  this.hide();
                   },
       showTime: isTime
      
  });
  cal.manageFields(id, id, format);
}

function showDivInfo(max, id, orderId) {
	for(var i = 1;i <= max; i++) {
		if(i == id) {
			if ($("#parent_element_"+i+"_"+orderId).is(":hidden")) {
        		$("#parent_element_"+i+"_"+orderId).fadeIn("slow");
        		$("#child_element_"+i+"_"+orderId).slideDown("slow");
      		} else {
      			$("#parent_element_"+i+"_"+orderId).fadeOut("slow");
      			$("#child_element_"+i+"_"+orderId).slideUp("slow");
      		}
		} else {
			$("#parent_element_"+i+"_"+orderId).fadeOut("slow");
			$("#child_element_"+i+"_"+orderId).slideUp("slow");
		}
	}
}

