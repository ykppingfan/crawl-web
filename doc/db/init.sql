-- 用户
create table if not exists sys_admin (
    id              	int(11) 			not null auto_increment,
    created_at      	datetime 			default null,
    creator         	varchar(255) 		default null,
    modifier        	varchar(255) 		default null,
    updated_at      	datetime 			default null,
    role_id         	int(11) 			default null,
    user_name       	varchar(255) 		default null,
    user_pwd        	varchar(255) 		default null,
    last_login_ip   	varchar(50) 		default null,
    last_login_time 	datetime 			default null,
    fail_number     	int(10) 			default null,
    disable_flag    	tinyint(2) 			default null,
    login_key       	varchar(20) 		default null,
    remark          	varchar(255) 		default null,
    primary key (id)
) engine=innodb default charset =utf8;

-- 系统日志
create table if not exists sys_log (
    id              int(10) 		not null auto_increment,
    created_at      datetime 		default null,
    creator         varchar(255) 	default null,
    modifier        varchar(255) 	default null,
    updated_at      datetime 		default null,
    oper_desc       varchar(2048) 	default null,
    oper_ip         varchar(255) 	default null,
    oper_type       tinyint(2) 		default null,
    user_id         int(10) 		default null,
    primary key (id)
) engine=innodb default charset =utf8;

-- 系统配置
create table if not exists sys_config (
    id              int(11) 		not null auto_increment,
    created_at      datetime 		default null,
    creator         varchar(255) 	default null,
    modifier        varchar(255) 	default null,
    updated_at      datetime 		default null,
    config_key      varchar(255) 	default null,
    config_value    varchar(255) 	default null,
    show_name       varchar(255) 	default null,
    remark          varchar(255) 	default null,
    primary key (id)
) engine=innodb default charset =utf8;

-- 标签表
create table if not exists p2p_label (
  id		    		int(11) 		not null auto_increment,  
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  title         		varchar(100)	default null comment 			'标签名称',
  content       		varchar(200) 	default null comment 			'标签内容',
  logo_word          	varchar(2) 		default null comment 			'标签logo',
  commend_flag  		tinyint(3) 		default '0' comment 			'是否为推荐标志',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台表
create table if not exists p2p_platform (
  id         			int(10) 					not null auto_increment,
  creator    			varchar(100) 				default null comment 			'创建者',
  created_at 			datetime 					default null comment 			'上线时间',
  modifier   			varchar(100) 				default null comment 			'修改者',
  updated_at 			datetime  					default null comment 			'修改时间',
  enterprise_id 		int(11)  					default null comment 			'机构资源id，预留',
  full_name 			varchar(200) 				default null comment 			'机构全称',
  short_name 			varchar(200)				default null comment 			'机构简称',
  legal_person 			varchar(200)				default null comment 			'平台法人',
  linkman 				varchar(200)				default null comment 			'平台联系人',
  mobile 				varchar(20) 				default null comment 			'电话',
  email 				varchar(100)				default null comment 			'邮箱',
  qq 					varchar(20) 				default null comment 			'qq',
  logo 					varchar(200) 				default null comment 			'机构logo',
  first_letter 			varchar(255) 					default null comment 			'首字母',
  english_name 			varchar(100) 				default null comment 			'英文全称',  
  commend_flag			tinyint(3) 					default '0' comment 			'是否为推荐平台',
  province_id 			int(11) 					default null comment 			'平台所在地--省id',
  city_id 				int(11) 					default null comment 			'平台所在市--市id',
  registered_capital 	varchar(50) 				default null comment 			'注册资金',
  up_time				datetime  					default null comment 			'上线时间',
  profit_range_begin  	varchar(20) 				default null comment 			'收益范围起',
  profit_range_end  	varchar(20) 				default null comment 			'收益范围止',
  profit_average 		int(11) 					default null comment 			'平均收益',
  security_mode			int(11) 					default null comment 			'保障模式',
  invest_term			int(11) 					default null comment 			'投资期限',
  funds_trust			int(11) 					default null comment 			'资金托管',
  debenture_transfer	int(11) 					default null comment 			'债权转让',
  auto_bid_flag 		tinyint(3) 					default null comment 			'是否自动投标', 
  background 			int(11) 					default null comment 			'平台背景',
  introduce 			varchar(1000)  			default null comment 			'平台简介',
  company_info 			varchar(1000)  			default null comment 			'公司信息',
  top_days				int(11) 					default null comment 			'剩余置顶天数--预留',
  refresh_num			int(11) 					default null comment 			'剩余刷新次数--预留',
  delete_flag 			tinyint(3) 					default '0' comment 			'删除标志',
  parameter 			varchar(100) 				default null comment 			'平台参数',  
  visit_count			int(11) 					default '0' comment 			'点击量',
  ad_flag   			tinyint(3) 					default null comment 			'是否广告位',
  url       			varchar(200) 				default null comment 			'平台网络地址',  
  top_flag              tinyint(3)                  DEFAULT '0'  COMMENT           '是否置顶',
  disable_flag			tinyint(3) 					DEFAULT '0'  COMMENT 			'是否禁用',
  quotation_flag		tinyint(3) 					DEFAULT '0'  COMMENT 			'是否展示行情',
  page_title 			varchar(255) 				DEFAULT NULL COMMENT 			'标题',
  page_keywords 		varchar(255) 				DEFAULT NULL COMMENT 			'关键字',
  page_description 		varchar(255) 				DEFAULT NULL COMMENT 			'描述',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台标签表
create table if not exists p2p_platform_label (
  id		    		int(11) 		not null auto_increment,   		
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id         	int(11) 		default null comment 			'平台id',
  label_id       		int(11) 		default null comment 			'标签id',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台项目类型表
create table if not exists p2p_platform_product_type (
  id		    		int(11) 		not null auto_increment,  
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id         	int(11) 		default null comment 			'平台id',
  product_type       	int(11) 		default null comment 			'项目类型',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台投资期限表
create table if not exists p2p_platform_product_period (
  id		    		int(11) 		not null auto_increment,   
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id         	int(11) 		default null comment 			'平台id',
  product_period       	int(11) 		default null comment 			'项目投资周期',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 产品表
create table if not exists p2p_product (
  id		    		int(11) 		not null auto_increment,   
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id 			int(11)  		default null comment 			'所属平台id',
  title 				varchar(100) 	default null comment 			'产品名称',
  pay_option  			int(11) 		default null comment 			'还款方式',
  profit 				double 			default null comment 			'年化收益率',
  invest_term 			int(11) 		default null comment 			'投资期限',
  progress 				int(11) 		default null comment 			'进度--预留字段',
  complete_flag 		tinyint(3) 		default null comment 			'是否完成',
  begin_money 			int(11) 		default null comment 			'起投金额',
  url       			varchar(100) 	default null comment 			'产品链接地址', 
  top_flag 				tinyint(3) 		default null comment 			'置顶标志',  
  visit_count			int(11) 		default null comment 			'点击量',
  commend_flag			tinyint(3) 		default null comment 			'删除标志',
  audit_flag  			tinyint(3) 		default null comment 			'删除标志',
  delete_flag 			tinyint(3) 		default null comment 			'删除标志',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 地区表
create table `p2p_region` (
  id		    		int(11) 		not null auto_increment,   
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  parent_id             int(11) 		default null comment            '上级区域id',
  data_level 			int(11) 		default null comment			'区域级别',
  data_name 			varchar(100) 	default null comment			'区域名称',
  primary key (`id`),
  key `idx_parent_id` (`parent_id`),
  key `idx_data_level` (`data_level`)
) engine=innodb default charset=utf8;

-- 平台排名表
create table if not exists p2p_ranking (
  id		    		int(11) 		not null auto_increment,  
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id 			int(11)  		default null comment 			'所属平台id',
  composite  			int(11) 		default null comment 			'综合排名',
  popularity 			int(11) 		default null comment 			'人气排名',
  traffic	 			int(11) 		default null comment 			'流量排名',
  praise 				int(11) 		default null comment 			'口碑排名',
  power		 			int(11) 		default null comment 			'实力排名',
  trade_num				int(11) 		default null comment 			'交易量排名',
  interest_rate			int(11) 		default null comment 			'利率排名',
  invest_passengers		int(11) 		default null comment 			'投资人次排名',
  loan_passengers		int(11) 		default null comment 			'借款人次排名',
  invest_money			int(11) 		default null comment 			'人均投资金额排名',
  loan_money			int(11) 		default null comment 			'人均借款金额排名',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 产品抢购日志表
create table if not exists p2p_product_buylog (
  id		    		int(11) 		not null auto_increment,   
  member_id       		int(11)		 	default null comment   			'用户id',
  buy_time	    		datetime 		default null comment 			'抢购时间',
  ip		      		varchar(50) 	default null comment 			'用户ip',
  platform_id         	int(11) 		default null comment 			'平台id',
  product_id        	int(11) 		default null comment 			'产品id',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台数据走势表
create table if not exists p2p_quotation (
  id		    		int(11) 		not null auto_increment,   
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id         	int(11) 		default null comment 			'平台id',
  data_time     		datetime  		default null comment 			'数据日期',
  trade_num		       	double			default null comment 			'成交量',
  interest_rate			double			default null comment 			'利率',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

-- 平台借款期限、借款金额表
create table if not exists p2p_quotation_percent (
  id		    		int(11) 		not null auto_increment,  
  creator       		varchar(100) 	default null comment   			'创建者',
  created_at    		datetime 		default null comment 			'创建时间',
  modifier      		varchar(100) 	default null comment 			'修改者',
  updated_at    		datetime  		default null comment 			'修改时间',
  platform_id         	int(11) 		default null comment 			'平台id',
  p1		 			bigint(20) 		default null comment 			'0-3月标总金额',
  p2	 				bigint(20) 		default null comment 			'3-6',
  p3		 			bigint(20) 		default null comment 			'6-12',
  p4					bigint(20) 		default null comment 			'一年以上',
  m1					bigint(20) 		default null comment 			'0-5万标个数',
  m2					bigint(20) 		default null comment 			'5-10',
  m3					bigint(20) 		default null comment 			'10-50',
  m4					bigint(20) 		default null comment 			'50-100',
  m5					bigint(20) 		default null comment 			'100万以上',
  primary key (id)
) engine =innodb default charset =utf8 row_format = compressed;

insert into sys_admin (role_id,user_name,disable_flag) values (-1,'admin',0);
insert into sys_admin (role_id,user_name,disable_flag) values (-1,'guoxingye',0);

insert into sys_config(config_key,config_value,show_name,remark) values 
('loginFailMaxCount','3','登录错误最大次数','密码输错超过此值将出现图片验证码'),
('checkAuthCode','1','是否启用谷歌身份认证','0为禁用，1为启用');


CREATE TABLE `p2p_template_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(60) DEFAULT NULL,
  `config_value` varchar(500) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `config_file` longtext,
  `created_at` datetime DEFAULT NULL,
  `creator` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modifier` varchar(20) DEFAULT NULL,
  `static_url` VARCHAR(255) DEFAULT null,
  `is_remote` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;


CREATE TABLE `p2p_generate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modifier` varchar(255) DEFAULT NULL,
  `config_key` varchar(255) DEFAULT NULL COMMENT '关键字',
  `type` tinyint(4) DEFAULT NULL COMMENT '模版类型',
  `config_value` varchar(5000) DEFAULT NULL COMMENT '模版内容',
  `config_param` varchar(255) DEFAULT NULL COMMENT '模版参数 json',
  `ftl_name` varchar(255) DEFAULT NULL COMMENT '生成模版 ftl 文件名',
  `html_name` varchar(255) DEFAULT NULL COMMENT '生成静态 文件名',
  `disable_flag` tinyint(4) DEFAULT NULL COMMENT '禁用标识',
  `describe` varchar(1000) DEFAULT NULL COMMENT '描述',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `p2p_generate` */

insert  into `p2p_generate`(`id`,`created_at`,`creator`,`updated_at`,`modifier`,`config_key`,`type`,`config_value`,`config_param`,`ftl_name`,`html_name`,`disable_flag`,`describe`) values (1,'2015-05-19 16:13:17','测试零','2015-05-19 20:54:39','测试零','topPlat5',1,'<ul class=\"products\">\r\n	<#if platlist0?exists && platlist0?size gt 0>\r\n		<#list platlist0 as platform>\r\n			<li <#if platform_index != 0>class=\"bg0${platform_index}\"</#if>>\r\n				<div class=\"logo\">\r\n					<a href=\"${p2pDomain!\'\'}/s/pingtai/${(platform.id)!}__1\" target=\"_blank\" class=\"avatar\"><img src=\"${resourceDomain!\'\'}/${(platform.logo)!}\" alt=\"${(platform.shortName)!}\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">${(platform.profitRangeBegin)!}%-${(platform.profitRangeEnd)!}%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span>${(platform.investTermStr)!}</span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"${p2pDomain!\'\'}/s/pingtai/${(platform.id)!}__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n		</#list>\r\n	</#if>\r\n</ul>','[{\"pageSize\":5}]','topPlat5.ftl','topPlat5.html',NULL,'置顶5条平台数据'),(2,'2015-05-19 17:58:36','测试零','2015-05-19 19:11:42','测试零','ranking10',3,'<div class=\"mr-comm mr-type\">\r\n    <div class=\"hd\">\r\n        <li>综合<br>排名</li>\r\n        <li>收益<br>排名</li>\r\n    </div>\r\n    <div class=\"mr-tit\">\r\n        <h3><a href=\"#\" target=\"_blank\">平台排名</a></h3>\r\n    </div>\r\n    <div class=\"bd\">\r\n        <div class=\"clearfix\">\r\n            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n                <tr>\r\n                    <th width=\"20%\">排名</th>\r\n                    <th>平台名称</th>\r\n                    <th>综合指数</th>\r\n                </tr>\r\n                <#if ranking0 ?? && ranking0?size gt 0>\r\n					<#list ranking0 as rateRank>\r\n						 <tr <#if rateRank_index % 2 == 0>class=\"bg\"</#if>>\r\n		                    <td><em <#if rateRank_index < 3>class=\"bgred\"</#if>>${rateRank_index + 1}</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"${p2pDomain!\'\'}/s/pingtai/${(rateRank.platformId)!}__1\" target=\"_blank\" title=\"\">${(rateRank.shortName)!}</a></td>\r\n		                    <td align=\"center\">${(rateRank.composite)!}</td>\r\n		                </tr>\r\n                	</#list>\r\n                </#if>\r\n            </table>\r\n        </div>\r\n        <div class=\"clearfix\">\r\n            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n                <tr>\r\n                    <th width=\"20%\">排名</th>\r\n                    <th>平台名称</th>\r\n                    <th>收益指数</th>\r\n                </tr>\r\n              	<#if ranking1 ?? && ranking1?size gt 0>\r\n					<#list ranking1 as rateRank>\r\n						 <tr <#if rateRank_index % 2 == 0>class=\"bg\"</#if>>\r\n		                    <td><em <#if rateRank_index < 3>class=\"bgred\"</#if>>${rateRank_index + 1}</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"${p2pDomain!\'\'}/s/pingtai/${(rateRank.platformId)!}__1\" target=\"_blank\" title=\"\">${(rateRank.shortName)!}</a></td>\r\n		                    <td align=\"center\">${(rateRank.interestRate)!}</td>\r\n		                </tr>\r\n                	</#list>\r\n                </#if>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>','[{\"pageSize\":5,\"orderBy\":\" r.composite desc \"},{\"pageSize\":5,\"orderBy\":\" r.interest_rate desc \"}]','ranking10.ftl','ranking10.html',NULL,'排行前10的平台'),(3,'2015-05-19 18:40:45','测试零','2015-05-19 20:38:03','测试零','comProduct5',2,'<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n		    	<tr>\r\n		    		<th width=\"16%\">平台</th>\r\n		    		<th class=\"tl\">产品名称</th>\r\n		    		<th class=\"tl\">年化收益率</th>\r\n		    		<th>投资期限</th>\r\n		    		<th>起投金额</th>\r\n		    		<th>还款方式</th>\r\n		    		<th>进度</th>\r\n		    		<th>操作</th>\r\n		    	</tr>\r\n		    	<#if (page0.result)?? && page0.result?size gt 0>\r\n                	<#list page0.result as product>\r\n		    			<tr <#if product_index % 2 == 0>class=\"bg\"</#if>>\r\n		    				<td>\r\n		    					<div class=\"logo\"><a href=\"${p2pDomain!}/s/pingtai/${(product.platformId)!}__1\" target=\"_blank\"><img src=\"${resourceDomain!\'\'}${(product.platformLogo)!}\" alt=\"${(product.shortName)!}\" /> </div></a> \r\n		    				</td>\r\n		    				<td class=\"tit tl\"><a href=\"javascript:buy(${(product.platformId)}, ${(product.id)!}, \'${(product.url)!}\')\" title=\"${(product.title)!}\">${(product.title)!}</a></td> \r\n		    				<td class=\"income tl\">${(product.profit)!}%</td>\r\n		    				<td>${(product.investTermStr)!}</td>\r\n		    				<td>￥${(product.beginMoney)!}</td>\r\n		    				<td>${(product.payOptionStr)!}</td>\r\n		    				<td class=\"red\"><#if (product.completeFlag)?? && product.completeFlag==1>已完成<#else>进行中</#if></td>\r\n		    				<td class=\"btn\"><a href=\"javascript:buy(${(product.platformId)}, ${(product.id)!}, \'${(product.url)!}\')\">抢标</a></td>\r\n		    			</tr>\r\n		    		</#list>\r\n		    	</#if>\r\n<#if (page1.result)?? && page1.result?size gt 0>\r\n                	<#list page1.result as product>\r\n		    			<tr <#if product_index % 2 == 0>class=\"bg\"</#if>>\r\n		    				<td>\r\n		    					<div class=\"logo\"><a href=\"${p2pDomain!}/s/pingtai/${(product.platformId)!}__1\" target=\"_blank\"><img src=\"${resourceDomain!\'\'}${(product.platformLogo)!}\" alt=\"${(product.shortName)!}\" /> </div></a> \r\n		    				</td>\r\n		    				<td class=\"tit tl\"><a href=\"javascript:buy(${(product.platformId)}, ${(product.id)!}, \'${(product.url)!}\')\" title=\"${(product.title)!}\">${(product.title)!}</a></td> \r\n		    				<td class=\"income tl\">${(product.profit)!}%</td>\r\n		    				<td>${(product.investTermStr)!}</td>\r\n		    				<td>￥${(product.beginMoney)!}</td>\r\n		    				<td>${(product.payOptionStr)!}</td>\r\n		    				<td class=\"red\"><#if (product.completeFlag)?? && product.completeFlag==1>已完成<#else>进行中</#if></td>\r\n		    				<td class=\"btn\"><a href=\"javascript:buy(${(product.platformId)}, ${(product.id)!}, \'${(product.url)!}\')\">抢标</a></td>\r\n		    			</tr>\r\n		    		</#list>\r\n		    	</#if>\r\n		    </table>',' [{\"pageSize\":5}]','comProduct5.ftl','comProduct5.html',NULL,'推荐产品'),(4,'2015-05-19 21:07:47','测试零','2015-05-19 21:07:47',NULL,'hotPlat15',1,'<div class=\"platform\">\r\n    <div class=\"w1000 clearfix\"> <strong>热门平台：</strong>\r\n        <div class=\"bd\">\r\n            <div>\r\n                <ul >\r\n<#if platlist0?exists && platlist0?size gt 0>\r\n    <#list platlist0 as platform>\r\n       <li><a href=\"${p2pDomain!\'\'}/s/pingtai/${(platform.id)!}__1\" target=\"_blank\">${(platform.shortName)!}</a></li>\r\n    </#list>\r\n</#if>\r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>','[{\"pageSize\":15}]','hotPlat15.ftl','hotPlat15.html',NULL,'热门平台');


insert  into `p2p_template_config`(`id`,`config_key`,`config_value`,`type`,`remark`,`config_file`,`created_at`,`creator`,`updated_at`,`modifier`,`static_url`,`is_remote`) values (4,'head','http://p2p.cngold.org/p2p_header.html',1,'P2P贷款静态_头部','<div class=\"header\" id=\"top\">\r\n	<!-- <div class=\"top-ad\">\r\n		<div class=\"w1000\"><script type=\"text/javascript\">BAIDU_CLB_fillSlot(\"1021979\");</script></div>\r\n		<a href=\"javascript:void(0);\" class=\"ad-close\">x</a>\r\n	</div> -->\r\n	<div class=\"head-top\">\r\n		<div class=\"w1000 clearfix\">\r\n			<span class=\"fl\">欢迎访问<a href=\"http://p2p.cngold.org\" target=\"_blank\">金投P2P</a>(p2p.cngold.org)！</span>\r\n			<span class=\"fl\"><a href=\"http://mingjia.cngold.org/expert/register.do\" target=\"_blank\"><em class=\"icon-manager\"></em>业务经理入驻</a></span>\r\n			<span class=\"fl\"><a href=\"http://jigou.cngold.org/company/back/register.htm\" target=\"_blank\"><em class=\"icon-company\"></em>企业入驻</a></span>\r\n			<span class=\"fr\"><a href=\"http://www.cngold.org/\" target=\"_blank\" class=\"blue\">< 返回金投网首页</a></span>\r\n			<span class=\"fr\"><a href=\"http://passport2.cngold.org/account/login.htm\" target=\"_blank\" class=\"btn-red\">登录</a><a href=\"http://passport2.cngold.org/account/add.htm\" target=\"_blank\" class=\"btn-gray\">注册</a></span>\r\n		</div>		\r\n	</div>\r\n	<div class=\"head-logo\">\r\n		<div class=\"w1000 clearfix\">\r\n			<h1><a href=\"http://www.cngold.org/\" target=\"_blank\">金投网</a></h1>\r\n			<h2><a href=\"http://p2p.cngold.org/\" target=\"_blank\"><span>P2P</span></a></h2>\r\n			<div class=\"fr\"><script type=\"text/javascript\">BAIDU_CLB_fillSlot(\"1065918\");</script></div>\r\n			<div class=\"fr\" style=\"margin-right:10px;\"><script type=\"text/javascript\">BAIDU_CLB_fillSlot(\"1065919\");</script></div>\r\n			<dl class=\"clearfix\">\r\n				<dt>7x24小时客服热线</dt>\r\n				<dd class=\"icon\">电话</dd>\r\n				<dd class=\"num\"><b>400-188-3535</b></dd>\r\n			</dl>\r\n		</div>		\r\n	</div>\r\n	<div class=\"head-sub-menu\">\r\n		<div class=\"w1000\"> \r\n			<ul class=\"clearfix head-menu fl\">\r\n				<li class=\"one\"><a href=\"http://p2p.cngold.org/\" target=\"_blank\">首页</a></li> \r\n				<li><a href=\"http://p2p.cngold.org/zixun/\" target=\"_blank\">P2P资讯</a></li> \r\n				<li><a href=\"http://p2p.cngold.org/s/daohang/\" target=\"_blank\">P2P导航</a></li>\r\n				<li><a href=\"http://p2p.cngold.org/s/paiming/\" target=\"_blank\">P2P排名</a></li>\r\n				<li><a href=\"http://p2p.cngold.org/s/pingtai/\" target=\"_blank\">P2P平台</a></li> \r\n				<li><a href=\"http://p2p.cngold.org/s/chanpin/\" target=\"_blank\">P2P产品</a></li>\r\n				<li><a href=\"http://p2p.cngold.org/gl/\" target=\"_blank\">P2P攻略</a></li> \r\n			</ul>\r\n			<ul class=\"more\">\r\n				<li class=\"topmenu-pop\"><a href=\"javascript:void(0);\" target=\"_blank\" class=\"arrow\">更多</a>\r\n					<ul style=\"display:none\">\r\n						<li>\r\n							<div class=\"clearfix\"> \r\n								<dl class=\"fl\"> \r\n									<dt><em class=\"icon01\"></em>热门产品</dt> \r\n									<dd><a href=\"http://kaihu.cngold.org/\" target=\"_blank\">贵金属开户</a></dd>\r\n									<dd><a href=\"http://energy.cngold.org/kaihu.html\" target=\"_blank\">原油开户</a></dd>\r\n									<dd><a href=\"http://forex.cngold.org/f_a_l\" target=\"_blank\">外汇开户</a></dd>    \r\n									<dd><a href=\"http://trust.cngold.org/t_lp/\" target=\"_blank\">信托产品</a></dd>\r\n									<dd><a href=\"http://credit.cngold.org/xyk/\" target=\"_blank\">信用卡办理</a></dd> \r\n									<dd><a href=\"http://loan.cngold.org/product/index.htm\" target=\"_blank\">贷款产品</a><sup class=\"new\"></sup></dd>\r\n									<dd><a href=\"http://p2p.cngold.org/\" target=\"_blank\">p2p理财</a><sup class=\"hot\"></sup></dd>   \r\n									<dd><a href=\"http://simu.cngold.org/\" target=\"_blank\">私募</a></dd> \r\n \r\n									<dt class=\"mt30\"><em class=\"icon02\"></em>热门搜索</dt> \r\n									<dd class=\"row2\"><a href=\"javascript:void(0);\">100万热门投资</a></dd> \r\n									<dd class=\"row2\"><a href=\"javascript:void(0);\">50-100万热门投资</a><sup class=\"hot\"></sup></dd> \r\n									<dd class=\"row2\"><a href=\"javascript:void(0);\">5%-10%收益率</a></dd> \r\n									<dd class=\"row2\"><a href=\"javascript:void(0);\">10%以上收益率</a></dd> \r\n								</dl> \r\n								<dl class=\"fl\"> \r\n									<dt><em class=\"icon03\"></em>产品服务</dt> \r\n									<dd><a href=\"http://kp.cngold.org/\" target=\"_blank\">看盘软件</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/down/4157.html\" target=\"_blank\">模拟交易</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/down/90.html\" target=\"_blank\">行情分析</a></dd> \r\n									<dd><a href=\"http://calendar.cngold.org/\" target=\"_blank\">财经日历</a></dd> \r\n									<dd><a href=\"http://jjh.cngold.org/\" target=\"_blank\">集金号</a><sup class=\"hot\"></sup></dd> \r\n									<dd><a href=\"http://dasai.cngold.org/\" target=\"_blank\">喊单大赛</a><sup class=\"hot\"></sup></dd> \r\n									<dd><a href=\"http://kp.cngold.org/\" target=\"_blank\">专家解盘</a></dd> \r\n									<dd><a href=\"http://kuaixun.cngold.org/\" target=\"_blank\">快讯</a></dd> \r\n									<dt class=\"mt30\"><em class=\"icon04\"></em>关于我们</dt> \r\n									<dd><a href=\"http://www.cngold.org/about/about.html\" target=\"_blank\">关于我们</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/about/service.html\" target=\"_blank\">收费服务</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/advservice/index.html\" target=\"_blank\">广告服务</a></dd> \r\n									<dd><a href=\"http://feedback.cngold.org/\" target=\"_blank\">建议反馈</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/about/duty.html\" target=\"_blank\">免责声明</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/about/link.html\" target=\"_blank\">友情链接</a></dd> \r\n									<dd><a href=\"http://www.cngold.org/about/contact.html\" target=\"_blank\">联系我们</a></dd> \r\n								</dl> \r\n								<dl class=\"fr\"> \r\n									<dt><em class=\"icon05\"></em>理财中心</dt> \r\n									<dd><a href=\"http://futures.cngold.org/\" target=\"_blank\">期货</a></dd>\r\n									<dd><a href=\"http://insurance.cngold.org/\" target=\"_blank\">保险</a></dd> \r\n									<dd><a href=\"http://zhubao.cngold.org/\" target=\"_blank\">珠宝</a></dd> \r\n									<dd><a href=\"http://bank.cngold.org/\" target=\"_blank\">银行</a></dd> \r\n									<dd><a href=\"http://usstock.cngold.org/\" target=\"_blank\">美股</a></dd> \r\n									<dd><a href=\"http://lux.cngold.org/\" target=\"_blank\">奢侈品</a></dd> \r\n									<dd><a href=\"http://finance.cngold.org/\" target=\"_blank\">财经</a></dd> \r\n									<dd><a href=\"http://cang.cngold.org/\" target=\"_blank\">收藏</a></dd> \r\n									<dd><a href=\"http://jiage.cngold.org/\" target=\"_blank\">价格</a></dd> \r\n									<dd><a href=\"http://jigou.cngold.org/\" target=\"_blank\">机构</a><sup class=\"hot\"></sup></dd> \r\n									<dd><a href=\"http://mingjia.cngold.org/\" target=\"_blank\">名家</a><sup class=\"hot\"></sup></dd> \r\n									<dd><a href=\"http://hao.cngold.org/\" target=\"_blank\">导航</a></dd> \r\n									<dt class=\"mt30\"><em class=\"icon06\"></em>新手指南</dt> \r\n									<dd><a href=\"http://www.cngold.org/baike/\" target=\"_blank\">金投攻略</a></dd> \r\n								</dl> \r\n							</div>\r\n						</li>\r\n					</ul>\r\n				</li>\r\n			</ul>\r\n		</div> \r\n	</div>\r\n</div>','2015-04-16 11:54:37','测试零','2015-05-19 21:08:16',NULL,'',1),(5,'foot','http://p2p.cngold.org/p2p_footer.html',1,'P2P贷款静态_底部','<div class=\"foot\">\r\n	<div class=\"w1000 clearfix\">\r\n		<div class=\"fl product\">\r\n			<h3><a href=\"http://www.cngold.org/fuwu.html\" target=\"_blank\">金投网产品服务中心</a></h3>\r\n			<ul class=\"clearfix first\">\r\n				<li><a href=\"http://www.cngold.org/money.html\" target=\"_blank\">理财产品</a><span>信托、私募、资管、有限合伙等产品的搜索、查找、对比、咨询、预约、购买</span></li>\r\n			</ul>\r\n			<ul class=\"clearfix row2\">\r\n				<li><a href=\"http://www.cngold.org/down/90.html\" target=\"_blank\">集金策略</a><span>策略指导建议</span></li>\r\n				<li><a href=\"http://jjh.cngold.org/\" target=\"_blank\">集 金 号</a><span>行情分析软件</span></li>\r\n				<li><a href=\"http://www.cngold.org/down/4157.html\" target=\"_blank\">模拟交易</a><span>最佳练兵场所</span></li>\r\n				<li><a href=\"http://kp.cngold.org/\" target=\"_blank\">在线解盘</a><span>实时行情播报</span></li>\r\n				<li><a href=\"http://dasai.cngold.org/\" target=\"_blank\">喊单大赛</a><span>拿现金大奖</span></li>\r\n				<li><a href=\"http://www.cngold.org/jintiao/\" target=\"_blank\">实物金条</a><span>全网最低价</span></li>\r\n				<li><a href=\"http://www.cngold.org/feinong/\" target=\"_blank\">聚焦非农</a><span>创造财富神话</span></li>\r\n			</ul>\r\n		</div>\r\n		<div class=\"fl about\">\r\n			<h3><a href=\"http://www.cngold.org/about/about.html\" target=\"_blank\">关于我们</a></h3>\r\n			<p>\r\n				<a href=\"http://www.cngold.org/about/about.html\" target=\"_blank\">关于我们</a>\r\n				<a href=\"http://www.cngold.org/about/service.html\" target=\"_blank\">收费服务</a>\r\n				<a href=\"http://www.cngold.org/about/Application-Services.html\" target=\"_blank\">应用服务</a>\r\n				<a href=\"http://feedback.cngold.org/\" target=\"_blank\">建议反馈</a>\r\n				<a href=\"http://www.cngold.org/data/sitemap.html\" target=\"_blank\">网站地图</a>\r\n				<a href=\"http://www.cngold.org/about/duty.html\" target=\"_blank\">免责声明</a>\r\n				<a href=\"http://www.cngold.org/about/link.html\" target=\"_blank\">友情链接</a>\r\n			</p>\r\n			<h3><a href=\"http://www.cngold.org/about/contact.html\" target=\"_blank\">联系我们</a></h3>\r\n			<dl class=\"clearfix service-tel\">\r\n				<dt>电话</dt>\r\n				<dd>7x24小时咨询热线</dd>\r\n				<dd class=\"number\">400-188-3535</dd>\r\n			</dl>\r\n		</div>\r\n		<div class=\"fl social\">\r\n			<h3 class=\"blue\">关注我们</h3>\r\n			<dl class=\"clearfix\">\r\n				<dt><img src=\"http://res.cngoldres.com/web/index/img/cngold_img_qrcode.png\" alt=\"微信二维码\"></dt>\r\n				<dd class=\"tit\">金投网官方微信</dd>\r\n				<dd class=\"des des_a\">微信扫描二维码，随时随地了解金投网最新动态，掌握第一手金融理财资讯。</dd>\r\n			</dl>\r\n			<div class=\"clearfix\">\r\n				<dl class=\"clearfix weibo\">\r\n					<dt class=\"sina\"><a href=\"http://weibo.com/cngold\" target=\"_blank\">新浪微博</a></dt>\r\n					<dd>新浪微博</dd>\r\n					<dd><a href=\"http://weibo.com/cngold\" target=\"_blank\">立即关注</a></dd>\r\n				</dl>\r\n				<dl class=\"clearfix weibo\">\r\n					<dt class=\"qq\"><a href=\"http://t.qq.com/cngoldorg\" target=\"_blank\">腾讯微博</a></dt>\r\n					<dd>腾讯微博</dd>\r\n					<dd><a href=\"http://t.qq.com/cngoldorg\" target=\"_blank\">立即关注</a></dd>\r\n				</dl>\r\n			</div>\r\n		</div>		\r\n	</div>\r\n	<div class=\"copyright\">\r\n		<p>版权所有 ©2008 - <#assign theYear = .now >${substring(theYear,2)} 金投网 www.cngold.org 浙ICP备09076998号 经营许可证编号：浙B2-20140239<br>本站信息仅供投资者参考，不做为投资建议！</p>\r\n	</div>\r\n</div>\r\n<div class=\"floatBox\">\r\n	<div class=\"floatBox_tit\">\r\n		<a href=\"javascript:void(0);\" onclick=\"$(\'.floatBox\').hide();\" class=\"closeBtn\">关闭</a>\r\n		<h3>在线客服</h3>\r\n		<p>点击按钮即可咨询</p>\r\n	</div>\r\n	<div class=\"floatBox_con\">\r\n		<ul class=\"clearfix\">\r\n		<li><a href=\"http://wpa.qq.com/msgrd?v=3&uin=2853517613&site=qq&menu=yes\" target=\"_blank\"><strong>咨询</strong>王经理<br><span>QQ:2853517613</span></a></li>\r\n		<li><a href=\"http://wpa.qq.com/msgrd?v=3&uin=2853517610&site=qq&menu=yes\" target=\"_blank\"><strong>咨询</strong>刘经理<br><span>QQ:2853517610</span></a></li>\r\n		<li><a href=\"http://wpa.qq.com/msgrd?v=3&uin=2853517614&site=qq&menu=yes\" target=\"_blank\"><strong>咨询</strong>鄢经理<br><span>QQ:2853517614</span></a></li>\r\n	</div>\r\n	<div class=\"floatBox_tel\">\r\n		24小时免费咨询热线\r\n		<strong>400-188-3535</strong>\r\n	</div>\r\n</div>\r\n<span style=\"display:none;\"><script language=\"javascript\" type=\"text/javascript\" src=\"http://js.users.51.la/2233024.js\"></script>\r\n<noscript><a href=\"http://www.51.la/?2233024\" target=\"_blank\"><img alt=\"我要啦免费统计\" src=\"http://img.users.51.la/2233024.asp\" style=\"border:none\" /></a></noscript></span>\r\n<script type=\"text/javascript\">\r\nvar _gaq = _gaq || [];\r\n  _gaq.push([\'_setAccount\', \'UA-4517813-2\']);\r\n  _gaq.push([\'_setDomainName\', \'.cngold.org\']);\r\n  _gaq.push([\'_setAllowHash\', false]);\r\n  _gaq.push([\'_trackPageview\']);\r\n\r\n  (function() {\r\n    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;\r\n    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';\r\n    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);\r\n  })();\r\n</script>\r\n<script type=\"text/javascript\">\r\n$(document).ready(function(){\r\n	//top adv\r\n	$(\".ad-close\").click(function(){\r\n		$(\".top-ad\").hide();\r\n	});\r\n</script>\r\n<!--<script type=\"text/javascript\">BAIDU_CLB_fillSlot(\"1005412\");</script>\r\n<script type=\"text/javascript\">BAIDU_CLB_fillSlot(\"1005413\");</script>-->','2015-04-16 11:55:59','测试零','2015-05-19 21:08:16',NULL,'',1),(9,'indexactivegl','http://p2p.cngold.org/act_gl.html',1,'P2P贷款静态_贷款攻略','<!-- 平台活动 -->\r\n		<div class=\"mr-comm activity\">\r\n			<div class=\"mr-tit\">\r\n				<span><a href=\"http://p2p.cngold.org/pthd/\" target=\"_blank\">更多 >></a></span>\r\n				<h3><a href=\"http://p2p.cngold.org/pthd/\" target=\"_blank\">平台活动</a></h3>\r\n			</div>\r\n			<div class=\"activity-con\">\r\n				<dl class=\"clearfix\">\r\n					<dt><a href=\"#\" target=\"_blank\"><img src=\"http://static.jinfuzi.com/edit/image/abvert/123456.jpg\" alt=\"金斧子\"></a></dt>\r\n					<dd class=\"bold fz14\"><a href=\"#\" target=\"_blank\">你我贷首投最高返现888元</a></dd>\r\n				</dl>\r\n				<ul class=\"list30\">\r\n					<li><a href=\"#\" target=\"_blank\">15- 4-14  安易融发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">爱定期I2015016期发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">大时贷上线周年庆投资人交流会截止</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">15- 4-14  安易融发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">015-04-14  迅航投资发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">爱上贷 4月14日发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">融亿通天下贷  注册就送50元红</a></li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n		<!-- 攻略 -->\r\n		<div class=\"mr-comm gl\">\r\n			<div class=\"mr-tit\">\r\n				<span><a href=\"http://p2p.cngold.org/gl/\" target=\"_blank\">更多 >></a></span>\r\n				<h3><a href=\"http://p2p.cngold.org/gl/\" target=\"_blank\">P2P攻略</a></h3>\r\n			</div>\r\n			<div class=\"activity-con\">\r\n				<dl class=\"clearfix wd\">\r\n					<dt><em>问</em><a href=\"#\" target=\"_blank\">哪儿买信托产品安全？</a></dt>\r\n					<dd><em>答</em><p>信托产品主要通过信托公司直销和第三方销售机构代销两种方式销售。</p></dd>\r\n				</dl>\r\n				<h4>热门攻略：</h4>\r\n				<ul class=\"list30\">\r\n					<li><a href=\"#\" target=\"_blank\">15- 4-14  安易融发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">爱定期I2015016期发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">大时贷上线周年庆投资人交流会截止</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">15- 4-14  安易融发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">015-04-14  迅航投资发标公告</a></li>\r\n					<li><a href=\"#\" target=\"_blank\">爱上贷 4月14日发标公告</a></li>\r\n				</ul>\r\n			</div>\r\n		</div>','2015-04-16 11:57:01','测试零','2015-05-19 21:08:16',NULL,'',1),(13,'indexnews','http://p2p.cngold.org/index_news.html',1,'P2P贷款静态_首页资讯','<div class=\"ml-comm top-news charts\">\r\n	<div class=\"hd\">\r\n		<ul class=\"clearfix\">\r\n			<li><a href=\"http://p2p.cngold.org/hyxw/\" target=\"_blank\">行业新闻</a></li>\r\n			<li><a href=\"http://p2p.cngold.org/ptdt/\" target=\"_blank\">平台动态</a></li>\r\n			<li><a href=\"http://p2p.cngold.org/yjbg/\" target=\"_blank\">研究报告</a></li>\r\n		</ul>\r\n	</div>\r\n	<div class=\"ml-tit\">\r\n		<h2><a href=\"http://p2p.cngold.org/zixun/\" target=\"_blank\">最新资讯</a><i>Top News</i></h2>\r\n	</div>\r\n	<div class=\"bd\">\r\n		<div class=\"clearfix\">\r\n			<dl class=\"clearfix\">\r\n				<dt>\r\n					<a href=\"#\" target=\"_blank\"><img src=\"img/p2p_banner01.jpg\" alt=\"\"></a>\r\n				</dt>\r\n				<dd>\r\n					<h4><a href=\"#\" target=\"_blank\">坏债债权被淘宝收入囊中 你怕了吗？</a></h4>\r\n					<p>1、据证券时报消息，淘宝网近日正式宣布成立资产处置平台，旨在为资产交易机构提供新的网络处置方式。今年3月，中国四大资产管理公司中的信达资产首度“触网”，通过淘宝资产</p>\r\n					<p>\r\n						<span>沙师弟</span>\r\n						<span>2015-4-21 01:15</span>\r\n					</p>\r\n				</dd>\r\n			</dl>\r\n			<ul class=\"list36 row2 fz14 clearfix\">\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财的特点</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财和银行理财</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托的特点有哪些</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">伞形结构化信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">什么是伞形信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托有风险吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">现在买信托安全吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么样</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎样买信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么买</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托产品可靠吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎么投资信托产品</a></li>\r\n			</ul>\r\n		</div>\r\n		<div class=\"clearfix\">\r\n			<dl class=\"clearfix\">\r\n				<dt>\r\n					<a href=\"#\" target=\"_blank\"><img src=\"img/p2p_banner01.jpg\" alt=\"\"></a>\r\n				</dt>\r\n				<dd>\r\n					<h4><a href=\"#\" target=\"_blank\">坏债债权被淘宝收入囊中 你怕了吗？</a></h4>\r\n					<p>1、据证券时报消息，淘宝网近日正式宣布成立资产处置平台，旨在为资产交易机构提供新的网络处置方式。今年3月，中国四大资产管理公司中的信达资产首度“触网”，通过淘宝资产</p>\r\n					<p>\r\n						<span>沙师弟</span>\r\n						<span>2015-4-21 01:15</span>\r\n					</p>\r\n				</dd>\r\n			</dl>\r\n			<ul class=\"list36 row2 fz14 clearfix\">\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财的特点</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财和银行理财</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托的特点有哪些</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">伞形结构化信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">什么是伞形信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托有风险吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">现在买信托安全吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么样</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎样买信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么买</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托产品可靠吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎么投资信托产品</a></li>\r\n			</ul>\r\n		</div>\r\n		<div class=\"clearfix\">\r\n			<dl class=\"clearfix\">\r\n				<dt>\r\n					<a href=\"#\" target=\"_blank\"><img src=\"img/p2p_banner01.jpg\" alt=\"\"></a>\r\n				</dt>\r\n				<dd>\r\n					<h4><a href=\"#\" target=\"_blank\">坏债债权被淘宝收入囊中 你怕了吗？</a></h4>\r\n					<p>1、据证券时报消息，淘宝网近日正式宣布成立资产处置平台，旨在为资产交易机构提供新的网络处置方式。今年3月，中国四大资产管理公司中的信达资产首度“触网”，通过淘宝资产</p>\r\n					<p>\r\n						<span>沙师弟</span>\r\n						<span>2015-4-21 01:15</span>\r\n					</p>\r\n				</dd>\r\n			</dl>\r\n			<ul class=\"list36 row2 fz14 clearfix\">\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财的特点</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财和银行理财</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托的特点有哪些</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">伞形结构化信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">什么是伞形信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托有风险吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">现在买信托安全吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么样</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎样买信托产品</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">信托理财产品怎么买</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">买信托产品可靠吗</a></li>\r\n				<li><a href=\"#\" target=\"_blank\" title=\"\">怎么投资信托产品</a></li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n</div>','2015-04-16 11:57:57','测试零','2015-05-19 21:08:16',NULL,'',1),(17,'banner','http://p2p.cngold.org/p2p_banner.html',1,'P2P贷款静态_banner','<div style=\"background:#fdce2d;\"><a href=\"#\" target=\"_blank\" title=\"12%最高年化收益率\"><img src=\"img/p2p_banner01.jpg\" alt=\"12%最高年化收益率\"></a></div>\r\n<div style=\"background:#fdce2d;\"><a href=\"#\" target=\"_blank\" title=\"12%最高年化收益率\"><img src=\"img/p2p_banner01.jpg\" alt=\"12%最高年化收益率\"></a></div>\r\n<div style=\"background:#fdce2d;\"><a href=\"#\" target=\"_blank\" title=\"12%最高年化收益率\"><img src=\"img/p2p_banner01.jpg\" alt=\"12%最高年化收益率\"></a></div>\r\n<div style=\"background:#fdce2d;\"><a href=\"#\" target=\"_blank\" title=\"12%最高年化收益率\"><img src=\"img/p2p_banner01.jpg\" alt=\"12%最高年化收益率\"></a></div>','2015-04-22 15:21:43','测试零','2015-05-19 21:08:16',NULL,'',1),(21,'ranking10','http://p2p.cngold.org/ranking10.html',1,'热门平台排行','<div class=\"mr-comm mr-type\">\r\n    <div class=\"hd\">\r\n        <li>综合<br>排名</li>\r\n        <li>收益<br>排名</li>\r\n    </div>\r\n    <div class=\"mr-tit\">\r\n        <h3><a href=\"#\" target=\"_blank\">平台排名</a></h3>\r\n    </div>\r\n    <div class=\"bd\">\r\n        <div class=\"clearfix\">\r\n            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n                <tr>\r\n                    <th width=\"20%\">排名</th>\r\n                    <th>平台名称</th>\r\n                    <th>综合指数</th>\r\n                </tr>\r\n                						 <tr class=\"bg\">\r\n		                    <td><em class=\"bgred\">1</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/1__1\" target=\"_blank\" title=\"\">红岭创投</a></td>\r\n		                    <td align=\"center\">830</td>\r\n		                </tr>\r\n						 <tr >\r\n		                    <td><em class=\"bgred\">2</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/3__1\" target=\"_blank\" title=\"\">PPmoney</a></td>\r\n		                    <td align=\"center\">720</td>\r\n		                </tr>\r\n						 <tr class=\"bg\">\r\n		                    <td><em class=\"bgred\">3</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/16__1\" target=\"_blank\" title=\"\">爱投资</a></td>\r\n		                    <td align=\"center\">700</td>\r\n		                </tr>\r\n						 <tr >\r\n		                    <td><em >4</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/15__1\" target=\"_blank\" title=\"\">你我贷</a></td>\r\n		                    <td align=\"center\">650</td>\r\n		                </tr>\r\n						 <tr class=\"bg\">\r\n		                    <td><em >5</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/7__1\" target=\"_blank\" title=\"\">积木盒子</a></td>\r\n		                    <td align=\"center\">640</td>\r\n		                </tr>\r\n            </table>\r\n        </div>\r\n        <div class=\"clearfix\">\r\n            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n                <tr>\r\n                    <th width=\"20%\">排名</th>\r\n                    <th>平台名称</th>\r\n                    <th>收益指数</th>\r\n                </tr>\r\n						 <tr class=\"bg\">\r\n		                    <td><em class=\"bgred\">1</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/1__1\" target=\"_blank\" title=\"\">红岭创投</a></td>\r\n		                    <td align=\"center\">434</td>\r\n		                </tr>\r\n						 <tr >\r\n		                    <td><em class=\"bgred\">2</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/10__1\" target=\"_blank\" title=\"\">有利网</a></td>\r\n		                    <td align=\"center\"></td>\r\n		                </tr>\r\n						 <tr class=\"bg\">\r\n		                    <td><em class=\"bgred\">3</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/9__1\" target=\"_blank\" title=\"\">人人贷</a></td>\r\n		                    <td align=\"center\"></td>\r\n		                </tr>\r\n						 <tr >\r\n		                    <td><em >4</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/8__1\" target=\"_blank\" title=\"\">金信网</a></td>\r\n		                    <td align=\"center\"></td>\r\n		                </tr>\r\n						 <tr class=\"bg\">\r\n		                    <td><em >5</em></td>\r\n		                    <td align=\"center\" class=\"tit\"><a href=\"http://p2p.cngold.org/s/pingtai/7__1\" target=\"_blank\" title=\"\">积木盒子</a></td>\r\n		                    <td align=\"center\"></td>\r\n		                </tr>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>','2015-05-13 18:35:26','测试零','2015-05-19 21:08:16',NULL,'ranking.html',1),(22,'topPlat5','http://p2p.cngold.org/topPlat5.html',1,'热门平台','<ul class=\"products\">\r\n				<li >\r\n				<div class=\"logo\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/285__1\" target=\"_blank\" class=\"avatar\"><img src=\"//upload/p2p/2015/5/19/9d377b10ce778c4938b3c7e2c63a229a.jpg\" alt=\"西西\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">1%-2%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span></span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/285__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n			<li class=\"bg01\">\r\n				<div class=\"logo\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/259__1\" target=\"_blank\" class=\"avatar\"><img src=\"//upload/loan/2015/0518/bdd07e918d4741e2ce72c1d17a8f630b.jpg\" alt=\"大华e贷\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">1%-2%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span></span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/259__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n			<li class=\"bg02\">\r\n				<div class=\"logo\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/225__1\" target=\"_blank\" class=\"avatar\"><img src=\"//upload/loan/2015/0518/50edb7069fb0783488c020bc8e3f1b91.jpg\" alt=\"惠车贷\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">1%-2%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span></span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/225__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n			<li class=\"bg03\">\r\n				<div class=\"logo\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/19__1\" target=\"_blank\" class=\"avatar\"><img src=\"//upload/loan/2015/0518/afe8b5df3a88876ee18cf7d8a2605851.jpg\" alt=\"88财富网\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">%-%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span></span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/19__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n			<li class=\"bg04\">\r\n				<div class=\"logo\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/176__1\" target=\"_blank\" class=\"avatar\"><img src=\"//upload/loan/2015/0518/3dc4231db89331fc9140ed731cf10de2.jpg\" alt=\"融信网\"></a>\r\n				</div>\r\n				<p>\r\n					<span>年化收益：</span>\r\n					<em class=\"fz18 bold\">%-%</em>\r\n				</p>\r\n				<p>\r\n					<span>期限范围：</span>\r\n					<span></span>\r\n				</p>\r\n				<div class=\"btn\">\r\n					<a href=\"http://p2p.cngold.org/s/pingtai/176__1\" target=\"_blank\"> 了解详情</a>\r\n				</div>\r\n			</li>\r\n</ul>','2015-05-13 18:51:04','测试零','2015-05-19 21:08:16',NULL,'topPlat.html',1),(24,'news','http://p2p.cngold.org/zixun_ssi.html',1,'咨询页面','<div class=\"mr-comm mr-news\">\r\n			<div class=\"mr-tit\">\r\n				<span><a href=\"#\" target=\"_blank\">更多</a></span>\r\n				<h3><a href=\"#\" target=\"_blank\">P2P资讯</a></h3>\r\n			</div>\r\n			<div class=\"hd\">\r\n				<ul class=\"clearfix\">\r\n					<li>推荐<em></em></li>\r\n					<li>热门<em></em></li>\r\n					<li>最新<em></em></li>\r\n					<li>随便看看<em></em></li>\r\n				</ul>\r\n			</div>\r\n			<div class=\"bd\">\r\n				<ul class=\"list30\">\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">怎么购买信托？购买信托都要去哪里购买呀</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n				</ul>\r\n				<ul class=\"list30\" style=\"display:none;\">\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？2</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">怎么购买信托？购买信托都要去哪里购买呀</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n				</ul>\r\n				<ul class=\"list30\" style=\"display:none;\">\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？3</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">怎么购买信托？购买信托都要去哪里购买呀</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n				</ul>\r\n				<ul class=\"list30\" style=\"display:none;\">\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？4</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">怎么购买信托？购买信托都要去哪里购买呀</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n					<li><a href=\"\" target=\"_blank\" title=\"\">目前国有信托投资公司有哪些？</a></li>\r\n				</ul>\r\n			</div>				\r\n		</div>','2015-05-19 13:06:02','测试零','2015-05-19 21:08:16',NULL,'',1),(25,'hotPlat15','http://p2p.cngold.org/hotPlat15.html',1,'顶部跑马灯热门平台','\r\n<div class=\"platform\">\r\n    <div class=\"w1000 clearfix\"> <strong>热门平台：</strong>\r\n        <div class=\"bd\">\r\n            <div>\r\n                <ul >\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/285__1\" target=\"_blank\">西西</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/259__1\" target=\"_blank\">大华e贷</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/225__1\" target=\"_blank\">惠车贷</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/19__1\" target=\"_blank\">88财富网</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/176__1\" target=\"_blank\">融信网</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/257__1\" target=\"_blank\">普惠理财</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/268__1\" target=\"_blank\">汉荣鼎盛</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/81__1\" target=\"_blank\">雪山贷</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/226__1\" target=\"_blank\">可溯贷</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/229__1\" target=\"_blank\">钱富通</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/30__1\" target=\"_blank\">合时代</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/245__1\" target=\"_blank\">51如易贷</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/181__1\" target=\"_blank\">360贷贷网</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/51__1\" target=\"_blank\">金银猫</a></li>\r\n       <li><a href=\"http://p2p.cngold.org/s/pingtai/255__1\" target=\"_blank\">优区贷</a></li>\r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>','2015-05-19 21:02:57','测试零','2015-05-19 21:08:16',NULL,'',1);
